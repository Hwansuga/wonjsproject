"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var node_kakao_1 = require("node-kakao");
var appConfig = require("./AppConfig_Check.json");
var ProcScript_1 = require("./ProcScript");
//onst request = require('request-promise-native').defaults({jar: true});
require('log-timestamp');
var schedule = require("node-schedule");
var RoomBanWinfo = /** @class */ (function () {
    function RoomBanWinfo() {
    }
    return RoomBanWinfo;
}());
var TargetHide = /** @class */ (function () {
    function TargetHide(chatid_, room_) {
        this.chatLog = chatid_;
        this.room = room_;
    }
    return TargetHide;
}());
var DEVICE_UUID = appConfig.DEVICE_UUID;
var DEVICE_NAME = appConfig.DEVICE_NAME;
var EMAIL = appConfig.EMAIL;
var PASSWORD = appConfig.PASSWORD;
var listBanWord = appConfig.listBanWord;
var Whitelist = appConfig.Whitelist;
var CLIENT = new node_kakao_1.TalkClient();
var procScript = new ProcScript_1.ProcScript();
var dicRoomMsgId = new Map();
function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}
function KickUser(room, user, nick_, limitCnt) {
    if (limitCnt === void 0) { limitCnt = 10; }
    return __awaiter(this, void 0, void 0, function () {
        var cntTry, workKick, _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 4, , 6]);
                    cntTry = 0;
                    _b.label = 1;
                case 1:
                    if (!true) return [3 /*break*/, 3];
                    console.log("Try Kick! / " + nick_);
                    return [4 /*yield*/, room.kickUser(user)];
                case 2:
                    workKick = _b.sent();
                    if (workKick.status == 0) {
                        console.log("Kick Success! / " + nick_);
                        return [3 /*break*/, 3];
                    }
                    else if (workKick.status == -500) {
                        console.log("not exist! / " + nick_);
                        return [3 /*break*/, 3];
                    }
                    else if (workKick.status == -810) {
                        console.log("already kicked / " + nick_);
                        return [3 /*break*/, 3];
                    }
                    if (cntTry > limitCnt) {
                        console.log("try over cnt! / " + nick_);
                        return [3 /*break*/, 3];
                    }
                    console.log("Kick Failed! status : " + workKick.status + "/ " + nick_);
                    wait(300);
                    cntTry++;
                    return [3 /*break*/, 1];
                case 3:
                    if (cntTry >= limitCnt) {
                        return [2 /*return*/, false];
                    }
                    else {
                        return [2 /*return*/, true];
                    }
                    return [3 /*break*/, 6];
                case 4:
                    _a = _b.sent();
                    console.log("Err Kick!!!!");
                    return [4 /*yield*/, Login()];
                case 5:
                    _b.sent();
                    return [2 /*return*/, false];
                case 6: return [2 /*return*/];
            }
        });
    });
}
function HideChat(room, chat, nickName, limitCnt) {
    if (limitCnt === void 0) { limitCnt = 30; }
    return __awaiter(this, void 0, void 0, function () {
        var cntTry, workHide, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 4, , 6]);
                    cntTry = 0;
                    _a.label = 1;
                case 1:
                    if (!true) return [3 /*break*/, 3];
                    console.log("Try Hide! / " + chat.text);
                    return [4 /*yield*/, room.hideChat(chat)];
                case 2:
                    workHide = _a.sent();
                    if (workHide.status == 0) {
                        console.log("Hide Chat Success! / " + nickName);
                        return [3 /*break*/, 3];
                    }
                    else if (workHide.status == -500) {
                        console.log("not exist! / " + nickName);
                        return [3 /*break*/, 3];
                    }
                    if (cntTry > limitCnt) {
                        console.log("try over cnt! / " + nickName);
                        return [3 /*break*/, 3];
                    }
                    console.log("Hide Chat Failed! status : " + workHide.status + "/ " + nickName);
                    wait(300);
                    cntTry++;
                    return [3 /*break*/, 1];
                case 3:
                    if (cntTry >= limitCnt) {
                        return [2 /*return*/, false];
                    }
                    else {
                        return [2 /*return*/, true];
                    }
                    return [3 /*break*/, 6];
                case 4:
                    err_1 = _a.sent();
                    console.log("Err Hide!!!! : " + err_1.Message);
                    return [4 /*yield*/, Login()];
                case 5:
                    _a.sent();
                    listHideTargetChat.push(new TargetHide(chat, room));
                    return [2 /*return*/, false];
                case 6: return [2 /*return*/];
            }
        });
    });
}
function GetChatLogId(room) {
    return __awaiter(this, void 0, void 0, function () {
        var channelInfo, valueInfo, lastChatId, lastChat, chatValue, targetChat;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, room.getLatestChannelInfo()];
                case 1:
                    channelInfo = _a.sent();
                    if (!channelInfo.success) return [3 /*break*/, 3];
                    valueInfo = channelInfo;
                    lastChatId = valueInfo.result.lastChatLog.prevLogId;
                    if (!(lastChatId != null && lastChatId != undefined)) return [3 /*break*/, 3];
                    return [4 /*yield*/, room.getChatListFrom(lastChatId)];
                case 2:
                    lastChat = _a.sent();
                    if (lastChat.success) {
                        console.log("LastChat : ", lastChat);
                        chatValue = lastChat;
                        targetChat = chatValue.result;
                        if (targetChat.length > 0) {
                            return [2 /*return*/, targetChat[0]];
                        }
                    }
                    _a.label = 3;
                case 3: return [2 /*return*/, null];
            }
        });
    });
}
function CheckHistory(room) {
    return __awaiter(this, void 0, void 0, function () {
        var roomName, banInfo, CheckedLogId, chatInfo, getChatRet, listChat_1, _loop_1, i, saveId;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    roomName = room.getDisplayName();
                    banInfo = listBanWord[0];
                    CheckedLogId = null;
                    if (!(dicRoomMsgId.has(room) == false)) return [3 /*break*/, 2];
                    return [4 /*yield*/, GetChatLogId(room)];
                case 1:
                    chatInfo = _a.sent();
                    if (chatInfo != null && chatInfo != undefined) {
                        dicRoomMsgId.set(room, chatInfo.prevLogId);
                    }
                    _a.label = 2;
                case 2:
                    CheckedLogId = dicRoomMsgId.get(room);
                    return [4 /*yield*/, room.getChatListFrom(CheckedLogId)];
                case 3:
                    getChatRet = _a.sent();
                    if (!getChatRet.success) return [3 /*break*/, 8];
                    listChat_1 = getChatRet.result;
                    _loop_1 = function (i) {
                        var openUerInfo, targetNick, banIndex, retKick, ret, retKick, ret;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    openUerInfo = room.getUserInfo(listChat_1[i].sender);
                                    targetNick = "";
                                    //매니져나 방장이면 패스
                                    if (openUerInfo == undefined || openUerInfo == null) {
                                    }
                                    else {
                                        targetNick = openUerInfo.nickname;
                                        if (openUerInfo.perm == node_kakao_1.OpenChannelUserPerm.MANAGER || openUerInfo.perm == node_kakao_1.OpenChannelUserPerm.OWNER) {
                                            console.log("perm : " + openUerInfo.perm.toString());
                                            return [2 /*return*/, "continue"];
                                        }
                                    }
                                    if (listChat_1[i].type != node_kakao_1.KnownChatType.REPLY) {
                                        return [2 /*return*/, "continue"];
                                    }
                                    if (!(listChat_1[i].type == node_kakao_1.KnownChatType.REPLY
                                        || listChat_1[i].type == node_kakao_1.KnownChatType.TEXT)) return [3 /*break*/, 4];
                                    banIndex = banInfo.banWord.findIndex(function (x) { return listChat_1[i].text.includes(x); });
                                    if (!(banIndex >= 0)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, KickUser(room, listChat_1[i].sender, targetNick, 1)];
                                case 1:
                                    retKick = _b.sent();
                                    console.log("text :  " + listChat_1[i].text + " / badword : " + banInfo.banWord[banIndex] + " / type : " + listChat_1[i].type + " / roomName : " + roomName + " / nickname : " + targetNick);
                                    return [4 /*yield*/, HideChat(room, listChat_1[i], targetNick, 2)];
                                case 2:
                                    ret = _b.sent();
                                    if (ret == false) {
                                        listHideTargetChat.push(new TargetHide(listChat_1[i], room));
                                    }
                                    _b.label = 3;
                                case 3: return [3 /*break*/, 7];
                                case 4:
                                    if (!(listChat_1[i].type == node_kakao_1.KnownChatType.STICKER
                                        || listChat_1[i].type == node_kakao_1.KnownChatType.DITEMEMOTICON
                                        || listChat_1[i].type == node_kakao_1.KnownChatType.STICKERGIF
                                        || listChat_1[i].type == node_kakao_1.KnownChatType.STICKERANI)) return [3 /*break*/, 7];
                                    if (!banInfo.banWord.includes("emoticon")) return [3 /*break*/, 7];
                                    console.log("emoticon! / roomName : " + roomName + " / nickname : " + targetNick);
                                    return [4 /*yield*/, KickUser(room, listChat_1[i].sender, targetNick, 1)];
                                case 5:
                                    retKick = _b.sent();
                                    return [4 /*yield*/, HideChat(room, listChat_1[i], targetNick, 2)];
                                case 6:
                                    ret = _b.sent();
                                    if (ret == false) {
                                        listHideTargetChat.push(new TargetHide(listChat_1[i], room));
                                    }
                                    _b.label = 7;
                                case 7: return [2 /*return*/];
                            }
                        });
                    };
                    i = 0;
                    _a.label = 4;
                case 4:
                    if (!(i < listChat_1.length)) return [3 /*break*/, 7];
                    return [5 /*yield**/, _loop_1(i)];
                case 5:
                    _a.sent();
                    _a.label = 6;
                case 6:
                    ++i;
                    return [3 /*break*/, 4];
                case 7:
                    if (listChat_1.length > 0) {
                        saveId = listChat_1[listChat_1.length - 1].prevLogId;
                        console.log("Save LogID : " + saveId);
                        dicRoomMsgId.set(room, saveId);
                    }
                    _a.label = 8;
                case 8: return [2 /*return*/];
            }
        });
    });
}
var listHideTargetChat = [];
function CheckChatMsg(room, data) {
    return __awaiter(this, void 0, void 0, function () {
        var roomName, banInfo, targetMsg;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    roomName = room.getDisplayName();
                    banInfo = listBanWord[0];
                    targetMsg = data.text;
                    console.log("msg : " + targetMsg + " , type : " + data.chat.type + " , id : " + data.chat.logId);
                    return [4 /*yield*/, CheckHistory(room)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, true];
            }
        });
    });
}
function ReplyTest(room, data) {
    return __awaiter(this, void 0, void 0, function () {
        var sender;
        return __generator(this, function (_a) {
            sender = data.getSenderInfo(room);
            console.log("User : " + JSON.stringify(sender));
            // var ret = await isKorean(room.channelId, sender.userId);
            // if (ret == true){
            //     console.log(`${sender.nickname} is Korean`);
            // }
            // else{
            //     console.log(`${sender.nickname} is foreigner`);
            // }
            return [2 /*return*/, true];
        });
    });
}
var timerID = null;
CLIENT.on("chat", function (data, channel) { return __awaiter(void 0, void 0, void 0, function () {
    var room, retCheckMsg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                room = CLIENT.channelList.open.get(channel.channelId);
                if (room == null) {
                    return [2 /*return*/];
                }
                return [4 /*yield*/, ReplyTest(room, data)];
            case 1:
                _a.sent();
                return [4 /*yield*/, CheckChatMsg(room, data)];
            case 2:
                retCheckMsg = _a.sent();
                if (retCheckMsg == true) {
                    return [2 /*return*/];
                }
                return [2 /*return*/];
        }
    });
}); });
CLIENT.on('message_hidden', function (hideLog, channel, feed) {
    console.log("Message " + hideLog.text + " hid from " + channel.getDisplayName() + " by " + hideLog.sender.userId);
});
CLIENT.on('chat_deleted', function (feedChatlog, channel, feed) {
    console.log(feedChatlog.text + " / " + feed.logId + " deleted by " + feedChatlog.sender.userId);
});
CLIENT.on('switch_server', function () {
    // Refresh credential and relogin client.
    Login().then(function () {
        console.log('Server switched!');
    });
});
CLIENT.on('disconnected', function (reason) {
    console.log("Disconnected!! reason: " + reason);
    Login().then(function () {
    });
});
function getLoginData() {
    return __awaiter(this, void 0, void 0, function () {
        var api, loginRes;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_kakao_1.AuthApiClient.create(DEVICE_NAME, DEVICE_UUID)];
                case 1:
                    api = _a.sent();
                    api.config.version = "3.4.2";
                    CLIENT.configuration.appVersion = '3.4.2.3187';
                    return [4 /*yield*/, api.login({
                            email: EMAIL,
                            password: PASSWORD,
                            forced: true
                        })];
                case 2:
                    loginRes = _a.sent();
                    if (!loginRes.success) {
                        throw new Error("Web login failed with status: " + loginRes.status);
                    }
                    return [2 /*return*/, loginRes.result];
            }
        });
    });
}
var session_info = '';
function Login() {
    return __awaiter(this, void 0, void 0, function () {
        var loginData, res;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getLoginData()];
                case 1:
                    loginData = _a.sent();
                    session_info = loginData.accessToken + "-" + loginData.deviceUUID;
                    return [4 /*yield*/, CLIENT.login(loginData)];
                case 2:
                    res = _a.sent();
                    if (res.success == true) {
                        console.log("CheckBot Run Success!!!!");
                    }
                    else {
                        process.exit();
                    }
                    return [2 /*return*/];
            }
        });
    });
}
// async function isKorean(channelId, userId)  : Promise<boolean> {
//     try {
//         await request({
//             uri: "https://gift-talk.kakao.com",
//             method: "POST",
//             form: {
//                 "x-kc-adid": "00000000-0000-0000-0000-000000000000",
//                 "agent": "aW9zLjE0LjcuMTo5LjQuNzoxMTI1eDI0MzY6aVBob25lOmlvcy4xNC43LjE%3D",
//                 "session_info": session_info,
//                 "chat_id": channelId?.toString(),
//                 "billingReferer": "talk_chatroom_plusbtn",
//                 "input_channel_id": "1926"
//             },
//             followAllRedirects: true,
//             headers: {
//                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
//                 'Content-Type': 'application/x-www-form-urlencoded'
//             },
//             resolveWithFullResponse: true
//         });
//         const response = await request(
//             {
//                 uri: "https://gift.kakao.com/a/v2/session/receivers",
//                 method: "POST",
//                 json: [{"serviceUserId": userId?.toString()}]
//             }
//         );
//         return response.validReceivers.length > 0
//     } catch (error) {
//         console.log(`Error ..Pass check. ${userId} / ${JSON.stringify(error)}`);
//         return true;
//     }
// }
var Working = false;
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var ruleRoom, roomJob, rule, job;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Login()];
                case 1:
                    _a.sent();
                    ruleRoom = new schedule.RecurrenceRule();
                    ruleRoom.second = [10, 20, 35, 50];
                    roomJob = schedule.scheduleJob(ruleRoom, function () { return __awaiter(_this, void 0, void 0, function () {
                        var _i, _a, key, room;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _i = 0, _a = Array.from(dicRoomMsgId.keys());
                                    _b.label = 1;
                                case 1:
                                    if (!(_i < _a.length)) return [3 /*break*/, 4];
                                    key = _a[_i];
                                    room = CLIENT.channelList.open.get(key.channelId);
                                    if (!(room != null && room != undefined)) return [3 /*break*/, 3];
                                    console.log("Check " + room.getDisplayName());
                                    return [4 /*yield*/, CheckHistory(room)];
                                case 2:
                                    _b.sent();
                                    _b.label = 3;
                                case 3:
                                    _i++;
                                    return [3 /*break*/, 1];
                                case 4: return [2 /*return*/];
                            }
                        });
                    }); });
                    rule = new schedule.RecurrenceRule();
                    rule.second = [0, 15, 30, 45];
                    job = schedule.scheduleJob(rule, function () { return __awaiter(_this, void 0, void 0, function () {
                        var arrSuccess, i, ret;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    if (listHideTargetChat.length > 0) {
                                        console.log("Do Schedule!");
                                    }
                                    else {
                                        //console.log(`Do Nothing`);
                                    }
                                    arrSuccess = [];
                                    i = 0;
                                    _a.label = 1;
                                case 1:
                                    if (!(i < listHideTargetChat.length)) return [3 /*break*/, 4];
                                    return [4 /*yield*/, HideChat(listHideTargetChat[i].room, listHideTargetChat[i].chatLog, "", 2)];
                                case 2:
                                    ret = _a.sent();
                                    if (ret == true) {
                                        arrSuccess.push(listHideTargetChat[i]);
                                    }
                                    wait(100);
                                    _a.label = 3;
                                case 3:
                                    ++i;
                                    return [3 /*break*/, 1];
                                case 4:
                                    listHideTargetChat = listHideTargetChat.filter(function (x) { return !arrSuccess.includes(x); });
                                    return [2 /*return*/];
                            }
                        });
                    }); });
                    return [2 /*return*/];
            }
        });
    });
}
main().then();
