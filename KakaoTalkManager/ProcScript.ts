
export class ProcData{
    public func = null;
    public param : any = null;
}

export default class ProcScript{

    listProc : ProcData[] = [];
    currentProcData : ProcData = null;

    DoneProc(){
        this.currentProcData = null;
        this.NextProc();
    }

    NextProc(){
        if (this.currentProcData == null && this.listProc.length > 0){
            this.currentProcData = this.listProc.shift();
            this.currentProcData.func();
        }
    }

    AddProc(func , param){
        let procData = new ProcData();
        procData.func = func;
        procData.param = param;

        this.listProc.push(procData);
        this.NextProc();
    }
}
export {};