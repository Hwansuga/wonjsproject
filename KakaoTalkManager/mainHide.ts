
import { AuthApiClient, ChatBuilder, KnownChatType, MentionContent, ReplyContent, TalkClient, TalkNormalChannel ,api, TalkOpenChannel, TalkChatData, TalkChannel, CommandResult, Chatlog, Long, ChannelUserInfo, ReplyAttachment, OpenChannelUserPerm, OpenChannelInfo, CommandResultDone, CommandResultFailed} from 'node-kakao';
import { chat } from 'node-kakao/dist/packet';
import * as appConfig from './AppConfig_Hide.json';
import  ProcScript  from './ProcScript'
import * as child_process from 'child_process';
import { getCheckinData } from 'node-kakao/dist/network';
import { wait } from './Util';

class RoomBanWinfo{
    roomName : string
    banWord : Array<string>
}

class UserChatInfo{
    id : Long
    nickname : string
    arrChat : Array<TalkChatData> = []
}

const DEVICE_UUID = appConfig.DEVICE_UUID;
const DEVICE_NAME = appConfig.DEVICE_NAME;
const EMAIL = appConfig.EMAIL;
const PASSWORD = appConfig.PASSWORD;

const listBanWord : Array<RoomBanWinfo> = appConfig.listBanWord;
const Whitelist : Array<string> = appConfig.Whitelist;

const NickBlacklist : Array<string> = appConfig.NickBlacklist;
const spam : boolean = appConfig.spam;
const spamRange : number = appConfig.spamRange;
const spamCount : number = appConfig.spamCount;

const limit : boolean = appConfig.limit;
const limitRange : number = appConfig.limitRange;
const limitCount : number = appConfig.limitCount;

const CLIENT = new TalkClient();
let procScript = new ProcScript();
let listUserChatInfo : Array<UserChatInfo> = [];

async function HideChat(room : TalkOpenChannel , chat : Chatlog , nickName : string , limitCnt : number = 30) {
    try{
        let cntTry = 0;
        while(true){
            console.log(`Try Hide! / ${chat.text}`);
            let workHide = await room.hideChat(chat);
            if (workHide.status == 0){
                console.log(`Hide Chat Success! / ${nickName}`);
                break;
            }

            if (cntTry > limitCnt){
                console.log(`try over cnt! / ${nickName}`);
                break;
            }

            console.log(`Hide Chat Failed! / ${nickName}`);
            wait(100);
            cntTry++;
        } 
        
        if (cntTry >= limitCnt){
            listHideTargetChat.push(chat);
        }
    }
    catch(err){
        console.log(`Err Hide!!!! : ${err.Message}`);
        await Login();
        // console.log(`Retry Hide ${chat.text}`);
        // await HideChat(room , chat , nickName);
    }      
}   

async function HideUserChat(room : TalkOpenChannel , userChatInfo : UserChatInfo) {
    for(let i=0 ; i<userChatInfo.arrChat.length ; ++i){
        await HideChat(room , userChatInfo.arrChat[i].chat , userChatInfo.nickname);
    }

    procScript.AddProc(()=>{
        let indexTarget = listUserChatInfo.indexOf(userChatInfo);
        if (indexTarget >= 0){
            listUserChatInfo.splice(indexTarget , 1);
        }
        procScript.DoneProc();
    } , null);
}

let listHideTargetChat : Array<Chatlog> = [];
async function CheckChatMsg(room : TalkOpenChannel , nickname : string , data : TalkChatData) : Promise<boolean> {
    let roomName = room.getDisplayName();
    

    let banInfo = listBanWord[0];
    let targetMsg = data.chat.text;
    let targetChat = data.chat;

    console.log(`msg : ${targetMsg} , type : ${data.chat.type} , id : ${data.chat.logId}`);
    
    if (data.chat.type == KnownChatType.TEXT 
    || data.chat.type == KnownChatType.PHOTO
    || data.chat.type == KnownChatType.REPLY 
    || data.chat.type == KnownChatType.OPEN_SCHEDULE 
    || data.chat.type == KnownChatType.OPEN_POST
    || data.chat.type == KnownChatType.CUSTOM
    || data.chat.type == KnownChatType.OPEN_VOTE){
        // if (data.chat.type == KnownChatType.TEXT){
        //     console.log(`${nickname} / Chat Data : ${JSON.stringify(data)}`);
        // }

        let banIndex = banInfo.banWord.findIndex(x=> targetMsg.includes(x));
        if (banIndex > -1){
            console.log(`text :  ${targetMsg} / badword : ${banInfo.banWord[banIndex]} / roomName : ${roomName} / nickname : ${nickname}`);
            await HideChat(room , targetChat , nickname);
            return true; 
        }       
    }
    else if (data.chat.type == KnownChatType.STICKERANI){
        console.log(`text :  ${targetMsg} / emoticon / roomName : ${roomName} / nickname : ${nickname}`);
        await HideChat(room , targetChat , nickname);
        return true;  
    }   
    return false;
}

function CollectChatInfo(data : TalkChatData , sender : ChannelUserInfo) : boolean {
    let userInfo = listUserChatInfo.find(x=>x.id == sender.userId);
    if (userInfo == null){
        userInfo = new UserChatInfo();
        userInfo.id = sender.userId;
        userInfo.nickname = sender.nickname;    
        listUserChatInfo.push(userInfo);   
    }
    userInfo.arrChat.push(data);

    return true;
}

function IsSpamUser(userInfo : UserChatInfo) : boolean{

    if (spam == false){
        return false;
    }
    if (userInfo.arrChat.length < spamCount){
        return false;
    }

    let endIndex = userInfo.arrChat.length-1;
    let startIndex = endIndex - spamRange;
    if (startIndex < 0){
        startIndex = 0;
    }
    let standardChat = userInfo.arrChat[endIndex];
    let listSpamRange = userInfo.arrChat.slice( startIndex );
    let cntSpam = listSpamRange.filter(x=>x.text == standardChat.text);
    if (cntSpam.length < spamCount){
        return false;
    }

    console.log(`${standardChat.text} 도배 검출. / count : ${spamCount} in ${spamRange}`);

    return true;
}

function  IsLimitOverUser(userInfo : UserChatInfo) {
    if (limit == false){
        return false;
    }

    if (userInfo.arrChat.length < limitCount){
        return false;
    }

    let endIndex = userInfo.arrChat.length-1;

    let standardChat = userInfo.arrChat[endIndex];
    let startTime = new Date( JSON.parse(JSON.stringify(standardChat.sendAt)) );
    startTime.setSeconds(startTime.getSeconds() - limitRange);
    let listInRageChat = userInfo.arrChat.filter(x=>x.sendAt >= startTime);
    if (listInRageChat.length < limitCount){
        return false;
    }

    console.log(`${standardChat.text} 제한속도 오버 검출. / count : ${limitCount} in ${limitRange} 초`);

    return true;
}

async function ReplyTest(room : TalkOpenChannel , nickname : string , data : TalkChatData) : Promise<boolean>{ 

    if (data.chat.text == `delTarget`){
        for(let i=0 ; i<listHideTargetChat.length ; ++i){
            await HideChat(room , listHideTargetChat[i] , `` , 2);
        }
    }
    return true;
}


let timerID : NodeJS.Timeout = null;

CLIENT.on(`chat` ,async (data , channel)=>{
    //FilterChat(data , channel);


    // if (timerID != null){
    //     clearTimeout(timerID);

    //     //메시지가 지정된 시간동안 없는 경우 프로세스 종료
    //     timerID = setTimeout(()=>{
    //         console.log(`No msg in ${appConfig.minCheckExit}min`);
    //         console.log(`${process.pid} exit`);
    //         process.exit(0);            
    //     }, appConfig.minCheckExit * 60 * 1000);
    // }

    let room : TalkOpenChannel = CLIENT.channelList.open.get(channel.channelId);
    if (room == null){
        return;
    } 
    
    let senderNickName = ``;
    let sender = data.getSenderInfo(channel);
    if (sender != null){
        let openUerInfo = room.getUserInfo(sender);
        //매니져나 방장이면 패스
        if (openUerInfo.perm == OpenChannelUserPerm.MANAGER || openUerInfo.perm == OpenChannelUserPerm.OWNER){
            return;
        }

        senderNickName = sender.nickname;
    } 

    //await ReplyTest(room , senderNickName , data);
    
    let exIndex = Whitelist.findIndex(x=>x == senderNickName);
    if (exIndex >= 0){
        return;
    } 

    let retCheckMsg = await CheckChatMsg(room , senderNickName , data);
    if (retCheckMsg == true){
        return;
    }

    if(sender != null && CollectChatInfo(data , sender)){
        
        let userInfo = listUserChatInfo.find(x=>x.id == sender.userId);
        if (userInfo == null){
            return false;
        }       

        if (IsSpamUser(userInfo)){
            console.log(`${sender.nickname} is SpamUser!`);
            await HideUserChat(room , userInfo);
            //KickUser(channel , sender);
            return;
        }

        if (IsLimitOverUser(userInfo)){
            console.log(`${sender.nickname} is LimitOverUser!`);
            await HideUserChat(room , userInfo);
            //KickUser(channel , sender);
            return;
        }
    }
});

CLIENT.on('message_hidden', (hideLog, channel, feed) => {
    console.log(`Message ${hideLog.text} hid from ${channel.getDisplayName()} by ${hideLog.sender.userId}`);
});

CLIENT.on('chat_deleted', (feedChatlog, channel, feed) => {
    console.log(`${feedChatlog.text} / ${feed.logId} deleted by ${feedChatlog.sender.userId}`);
});

CLIENT.on('switch_server', () => {
    // Refresh credential and relogin client.
    Login().then(() => {
      console.log('Server switched!');
    });
});

CLIENT.on('disconnected', (reason) => {
    console.log(`Disconnected!! reason: ${reason}`);
    Login().then(()=>{

    });
});

async function getLoginData(): Promise<api.LoginData> {
    const api = await AuthApiClient.create(DEVICE_NAME, DEVICE_UUID);
    api.config.version = `3.4.2`;
    CLIENT.configuration.appVersion = '3.4.2.3187';
    const loginRes = await api.login({
      email: EMAIL,
      password: PASSWORD,
      forced: true,
    });

    if (!loginRes.success){
        throw new Error(`Web login failed with status: ${loginRes.status}`);
    } 
  
    return loginRes.result;
}

let session_info : string = '';
async function Login() {
    const loginData = await getLoginData();
    session_info = `${loginData.accessToken}-${loginData.deviceUUID}`;
    const res = await CLIENT.login(loginData);

    if (res.success == true){
        console.log(`HideBot Run Success!!!!`);
    }
    else{
        process.exit();
    }  
}

let Working : Boolean = false;
async function  main() {
    await Login();

    //프로세스 재시작 등록
    // process.on("exit", (code) => {
    //     console.log(`code : ${code}`);
    //     console.log(`argv : ${process.argv}`);
    //     let child = child_process.spawn(process.argv.shift(), process.argv, {
    //         shell: true,
    //         detached : true
    //     });

    //     console.log(`child pid :  ${child.pid}`);     
    // });
    
}
main().then();