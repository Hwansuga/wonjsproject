"use strict";
exports.__esModule = true;
exports.ProcData = void 0;
var ProcData = /** @class */ (function () {
    function ProcData() {
        this.func = null;
        this.param = null;
    }
    return ProcData;
}());
exports.ProcData = ProcData;
var ProcScript = /** @class */ (function () {
    function ProcScript() {
        this.listProc = [];
        this.currentProcData = null;
    }
    ProcScript.prototype.DoneProc = function () {
        this.currentProcData = null;
        this.NextProc();
    };
    ProcScript.prototype.NextProc = function () {
        if (this.currentProcData == null && this.listProc.length > 0) {
            this.currentProcData = this.listProc.shift();
            this.currentProcData.func();
        }
    };
    ProcScript.prototype.AddProc = function (func, param) {
        var procData = new ProcData();
        procData.func = func;
        procData.param = param;
        this.listProc.push(procData);
        this.NextProc();
    };
    return ProcScript;
}());
exports["default"] = ProcScript;
