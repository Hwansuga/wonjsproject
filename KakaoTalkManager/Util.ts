import { Long, TalkChatData } from "node-kakao";


export function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
}

export class RoomBanWinfo{
   roomName : string
   banWord : Array<string>
}

export class UserChatInfo{
   id : Long
   nickname : string
   arrChat : Array<TalkChatData> = []
}