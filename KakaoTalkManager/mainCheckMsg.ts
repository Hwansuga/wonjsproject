
import { api, AuthApiClient, ChannelUser, ChatBuilder, Chatlog, CommandResultDone, KnownChatType, Long, OpenChannel, OpenChannelInfo, OpenChannelUserPerm, ReplyContent, TalkChatData, TalkClient, TalkOpenChannel } from 'node-kakao';
import * as appConfig from './AppConfig_Check.json';
import ProcScript from './ProcScript';
//onst request = require('request-promise-native').defaults({jar: true});
require('log-timestamp');

import * as schedule from 'node-schedule';

class RoomBanWinfo{
    roomName : string
    banWord : Array<string>
}

class TargetHide{
    constructor(chatid_ , room_){
        this.chatLog = chatid_;
        this.room = room_;
    }
    chatLog : Chatlog
    room : TalkOpenChannel;
}

const DEVICE_UUID = appConfig.DEVICE_UUID;
const DEVICE_NAME = appConfig.DEVICE_NAME;
const EMAIL = appConfig.EMAIL;
const PASSWORD = appConfig.PASSWORD;

const listBanWord : Array<RoomBanWinfo> = appConfig.listBanWord;
const Whitelist : Array<string> = appConfig.Whitelist;

const CLIENT = new TalkClient();
let procScript = new ProcScript();

let dicRoomMsgId : Map<TalkOpenChannel , Long> = new Map();

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
}

async function KickUser(room : TalkOpenChannel , user : ChannelUser,nick_:string, limitCnt : number = 10) : Promise<boolean> {

    try{
        let cntTry = 0;
        while(true){
            console.log(`Try Kick! / ${nick_}`);
            let workKick = await room.kickUser(user);
            if (workKick.status == 0){
                console.log(`Kick Success! / ${nick_}`);
                break;
            }
            else if (workKick.status == -500){
                console.log(`not exist! / ${nick_}`);
                break;
            }
            else if (workKick.status == -810){
                console.log(`already kicked / ${nick_}`);
                break;
            }

            if (cntTry > limitCnt){
                console.log(`try over cnt! / ${nick_}`);
                break;
            }

            console.log(`Kick Failed! status : ${workKick.status}/ ${nick_}`);
            wait(300);
            cntTry++;
        }

        if (cntTry >= limitCnt){
            return false;
        }
        else{
            return true;
        }

    }
    catch{
        console.log(`Err Kick!!!!`);
        await Login();

        return false;
    }   
}


async function HideChat(room : TalkOpenChannel , chat : Chatlog , nickName : string , limitCnt : number = 30) : Promise<boolean>{
    try{
        let cntTry = 0;
        while(true){
            console.log(`Try Hide! / ${chat.text}`);
            let workHide = await room.hideChat(chat);
            if (workHide.status == 0){
                console.log(`Hide Chat Success! / ${nickName}`);
                break;
            }
            else if (workHide.status == -500){
                console.log(`not exist! / ${nickName}`);
                break;
            }

            if (cntTry > limitCnt){
                console.log(`try over cnt! / ${nickName}`);
                break;
            }

            console.log(`Hide Chat Failed! status : ${workHide.status}/ ${nickName}`);
            wait(300);
            cntTry++;
        } 

        if (cntTry >= limitCnt){
            return false;
        }
        else{
            return true;
        }
    }
    catch(err){
        console.log(`Err Hide!!!! : ${err.Message}`);
        await Login();
        listHideTargetChat.push(new TargetHide(chat , room));
        return false;
        // console.log(`Retry Hide ${chat.text}`);
        // await HideChat(room , chat , nickName);
    }      
}   


async function GetChatLogId(room : TalkOpenChannel) : Promise<Chatlog>{
    let channelInfo = await room.getLatestChannelInfo();
    if (channelInfo.success){
        let valueInfo = channelInfo as CommandResultDone<OpenChannelInfo>;
        //console.log(`channelInfo : ` , JSON.stringify(valueInfo.result));
        let lastChatId = valueInfo.result.lastChatLog.prevLogId;
        if (lastChatId != null && lastChatId != undefined){
            var lastChat = await room.getChatListFrom(lastChatId);
            if (lastChat.success){
                console.log(`LastChat : ` , lastChat);
                let chatValue = lastChat as CommandResultDone<Chatlog[]>;
                let targetChat = chatValue.result;
                if (targetChat.length > 0){
                    return targetChat[0];                         
                }          
            }
        }     
    } 
    
    return null;
}

async function CheckHistory(room : TalkOpenChannel){
    let roomName = room.getDisplayName();  
    let banInfo = listBanWord[0];

    let CheckedLogId = null;
    if (dicRoomMsgId.has(room) == false){
        let chatInfo = await GetChatLogId(room);
        if (chatInfo != null && chatInfo != undefined){
            dicRoomMsgId.set(room , chatInfo.prevLogId);            
        }       
    }  

    CheckedLogId = dicRoomMsgId.get(room);
    
    let getChatRet = await room.getChatListFrom(CheckedLogId);
    if (getChatRet.success){
        let listChat = (getChatRet as CommandResultDone<Chatlog[]>).result;
        for(let i=0 ; i<listChat.length ; ++i){
            let openUerInfo = room.getUserInfo(listChat[i].sender);
            let targetNick : string = ``;

            //매니져나 방장이면 패스
            if (openUerInfo == undefined || openUerInfo == null){

            }
            else{
                targetNick = openUerInfo.nickname;
                if (openUerInfo.perm == OpenChannelUserPerm.MANAGER || openUerInfo.perm == OpenChannelUserPerm.OWNER){
                    console.log(`perm : ${openUerInfo.perm.toString()}`);
                    continue;
                }
            }
            
            if (listChat[i].type != KnownChatType.REPLY){
                continue;
            }

            if (listChat[i].type == KnownChatType.REPLY 
                || listChat[i].type == KnownChatType.TEXT){
                let banIndex = banInfo.banWord.findIndex(x=> listChat[i].text.includes(x));
                if (banIndex >= 0){
                                                           
                    let retKick = await KickUser(room , listChat[i].sender , targetNick , 1);
                                          
                    console.log(`text :  ${listChat[i].text} / badword : ${banInfo.banWord[banIndex]} / type : ${listChat[i].type} / roomName : ${roomName} / nickname : ${targetNick}`);
                         
                    let ret = await HideChat(room , listChat[i] , targetNick , 2);
                    if (ret == false){
                        listHideTargetChat.push(new TargetHide(listChat[i] , room));
                    }
                    
                }
            }
            else if (listChat[i].type == KnownChatType.STICKER 
                || listChat[i].type == KnownChatType.DITEMEMOTICON
                || listChat[i].type == KnownChatType.STICKERGIF
                || listChat[i].type == KnownChatType.STICKERANI){
                if (banInfo.banWord.includes(`emoticon`)){
                    console.log(`emoticon! / roomName : ${roomName} / nickname : ${targetNick}`);
                    let retKick = await KickUser(room , listChat[i].sender , targetNick , 1);
                    let ret = await HideChat(room , listChat[i] , targetNick , 2);
                    if (ret == false){
                        listHideTargetChat.push(new TargetHide(listChat[i] , room));
                    }
                }                
            }                
        }

        if (listChat.length > 0){
            let saveId = listChat[listChat.length -1].prevLogId;
            console.log(`Save LogID : ${saveId}`);
            dicRoomMsgId.set(room , saveId);
        }
    }     
}

let listHideTargetChat : Array<TargetHide> = [];
async function CheckChatMsg(room : TalkOpenChannel , data : TalkChatData) : Promise<boolean> {
    let roomName = room.getDisplayName();
    

    let banInfo = listBanWord[0];
    let targetMsg = data.text;

    console.log(`msg : ${targetMsg} , type : ${data.chat.type} , id : ${data.chat.logId}`);
    
    await CheckHistory(room);
    
    return true;
}

async function ReplyTest(room : TalkOpenChannel , data : TalkChatData) : Promise<boolean>{ 

    /*
    if (data.chat.text == `어떤걸`){
        
        for(let i=0 ; i<10 ; ++i){
            let chat = new ChatBuilder().text(`" 주목해주세요 "

            ▶️점심시간 긴급 공지◀️
            
            ​
            
            @@@###$$$%%%&&&***매수매도!!!
            
            ​
            
            다들 맛점들 하고 계신가요?
            
            식사시간에 죄송합니다
            
            저희방이 광고테러로 인하여 운영계정이
            
            정지가 되어버렸습니다 ..
            
            ​
            
            해서 불가피하게 이 곳은 운영중단하고
            
            빠른 오후장 대응 및 오전에 드린
            
            종목들 브리핑드리려 합니다.
            
            ​
            
            짧은 시간동안 이동하니
            
            모두 신속하게 움직여주세요!
            
            ​
            
            https://open.kakao.com/o/gCwakmId
            
            https://open.kakao.com/o/gCwakmId`)
            .append(new ReplyContent({
                type: 1,
                logId: new Long(null),
                prevLogId: new Long(null),
                sender: { userId: new Long(null) },
                sendAt: null,
                messageId: null
        })).build(KnownChatType.REPLY);

        chat.attachment = null;  
        let retSend = await room.sendChat(chat);

        console.log(`Send ${i}`);

        wait(1000);
        }

    }
    */

    const sender = data.getSenderInfo(room);

    console.log(`User : ${JSON.stringify(sender)}`);

    // var ret = await isKorean(room.channelId, sender.userId);
    // if (ret == true){
    //     console.log(`${sender.nickname} is Korean`);
    // }
    // else{
    //     console.log(`${sender.nickname} is foreigner`);
    // }


    return true;
}


let timerID : NodeJS.Timeout = null;

CLIENT.on(`chat` ,async (data , channel)=>{
    let room : TalkOpenChannel = CLIENT.channelList.open.get(channel.channelId);
    if (room == null){
        return;
    } 
    
    await ReplyTest(room , data);
    
    let retCheckMsg = await CheckChatMsg(room , data);
    if (retCheckMsg == true){
        return;
    }
});

CLIENT.on('message_hidden', (hideLog, channel, feed) => {
    console.log(`Message ${hideLog.text} hid from ${channel.getDisplayName()} by ${hideLog.sender.userId}`);
});

CLIENT.on('chat_deleted', (feedChatlog, channel, feed) => {
    console.log(`${feedChatlog.text} / ${feed.logId} deleted by ${feedChatlog.sender.userId}`);
});

CLIENT.on('switch_server', () => {
    // Refresh credential and relogin client.
    Login().then(() => {
      console.log('Server switched!');
    });
});

CLIENT.on('disconnected', (reason) => {
    console.log(`Disconnected!! reason: ${reason}`);
    Login().then(()=>{

    });
});

async function getLoginData(): Promise<api.LoginData> {
    const api = await AuthApiClient.create(DEVICE_NAME, DEVICE_UUID);   
    api.config.version = `3.4.2`;
    CLIENT.configuration.appVersion = '3.4.2.3187';
    const loginRes = await api.login({
      email: EMAIL,
      password: PASSWORD,
      forced: true,
    });

    if (!loginRes.success){
        throw new Error(`Web login failed with status: ${loginRes.status}`);
    } 
  
    return loginRes.result;
}

let session_info : string = '';
async function Login() {
    const loginData = await getLoginData();
    session_info = `${loginData.accessToken}-${loginData.deviceUUID}`;
    const res = await CLIENT.login(loginData);

    if (res.success == true){
        console.log(`CheckBot Run Success!!!!`);
    }
    else{
        process.exit();
    }  
}

// async function isKorean(channelId, userId)  : Promise<boolean> {
//     try {
//         await request({
//             uri: "https://gift-talk.kakao.com",
//             method: "POST",
//             form: {
//                 "x-kc-adid": "00000000-0000-0000-0000-000000000000",
//                 "agent": "aW9zLjE0LjcuMTo5LjQuNzoxMTI1eDI0MzY6aVBob25lOmlvcy4xNC43LjE%3D",
//                 "session_info": session_info,
//                 "chat_id": channelId?.toString(),
//                 "billingReferer": "talk_chatroom_plusbtn",
//                 "input_channel_id": "1926"
//             },
//             followAllRedirects: true,
//             headers: {
//                 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/105.0.0.0 Safari/537.36',
//                 'Content-Type': 'application/x-www-form-urlencoded'
//             },
//             resolveWithFullResponse: true
//         });

//         const response = await request(
//             {
//                 uri: "https://gift.kakao.com/a/v2/session/receivers",
//                 method: "POST",
//                 json: [{"serviceUserId": userId?.toString()}]
//             }
//         );

//         return response.validReceivers.length > 0
//     } catch (error) {
//         console.log(`Error ..Pass check. ${userId} / ${JSON.stringify(error)}`);
//         return true;
//     }
// }

let Working : Boolean = false;
async function  main() {
    await Login();

    // console.log(`openChannel : ${JSON.stringify(CLIENT.channelList.open.all)}`);
    // for (let i=0 ; i<CLIENT.channelList.open.all.length ; ++i){
    //     let room : TalkOpenChannel = CLIENT.channelList.open.all[i];
    //     let chatInfo = await GetChatLogId(room);
    //     console.log(`${room.getDisplayName()} , ${chatInfo.prevLogId}`);
    //     dicRoomMsgId.set(room , chatInfo.prevLogId);
    // }

    var ruleRoom = new schedule.RecurrenceRule();
    ruleRoom.second = [10 , 20 , 35 , 50]

    let roomJob = schedule.scheduleJob(ruleRoom , async()=>{
        for(let key of Array.from( dicRoomMsgId.keys()) ) {
            let room : TalkOpenChannel = CLIENT.channelList.open.get(key.channelId);
            if (room != null && room != undefined){
                console.log(`Check ${room.getDisplayName()}`);
                await CheckHistory(room);
            }            
        }
    });

    var rule = new schedule.RecurrenceRule();
    rule.second = [0,15,30,45];

    let job = schedule.scheduleJob(rule , async ()=>{
        if (listHideTargetChat.length > 0){
            console.log(`Do Schedule!`);
        }
        else{
            //console.log(`Do Nothing`);
        }
        
        let arrSuccess : Array<TargetHide> = [];
        for(let i=0 ; i<listHideTargetChat.length ; ++i){
            let ret = await HideChat(listHideTargetChat[i].room , listHideTargetChat[i].chatLog , `` , 2);
            if (ret == true){
                arrSuccess.push(listHideTargetChat[i]);
            }
            wait(100);
        }

        listHideTargetChat = listHideTargetChat.filter(x=> !arrSuccess.includes(x));
    });


    //프로세스 재시작 등록
    // process.on("exit", (code) => {
    //     console.log(`code : ${code}`);
    //     console.log(`argv : ${process.argv}`);
    //     let child = child_process.spawn(process.argv.shift(), process.argv, {
    //         shell: true,
    //         detached : true
    //     });

    //     console.log(`child pid :  ${child.pid}`);     
    // });
    
}
main().then();