import { AuthApiClient, KnownAuthStatusCode } from 'node-kakao';
import { resolve } from 'path/posix';
import * as readline from 'readline';
import * as appConfig from './AppConfig_Hide.json';

const DEVICE_UUID = appConfig.DEVICE_UUID;
const DEVICE_NAME = appConfig.DEVICE_NAME;
const EMAIL = appConfig.EMAIL;
const PASSWORD = appConfig.PASSWORD;


async function main(){
    console.log(`HideBot registaration ready!`);
    const form = {
        email : EMAIL,
        password : PASSWORD,
    };

    const api = await AuthApiClient.create(DEVICE_NAME, DEVICE_UUID);
    api.config.version = `3.4.2`;
    const loginRes = await api.login(form);
    if (loginRes.success){
        throw new Error(`Device already registered!`);
    }

    if (loginRes.status !== KnownAuthStatusCode.DEVICE_NOT_REGISTERED){
        throw new Error(`Web login failed with status : ${loginRes.status}`);
    }

    const passcodeRes = await api.requestPasscode(form);
    if (passcodeRes.success == false){
        throw new Error(`Passcode request failed with status : ${passcodeRes.status}`);
    }

    const inputInterface = readline.createInterface({
        input : process.stdin,
        output : process.stdout,
    });

    const passcode = await new Promise<string>((resolve) =>{
        inputInterface.question(`Enter passcode: `, resolve);
    });
    inputInterface.close();

    const registerRes = await api.registerDevice(form , passcode , true);
    if (registerRes.success == false){
        throw new Error(`Device registration failed with status : ${registerRes.status}`);
    }

    console.log(`Device ${DEVICE_UUID} has been registered`);

    const loginAfterRes = await api.login(form);
    if (loginAfterRes.success == false){
        throw new Error(`Web login failed with status : ${loginAfterRes.status}`);
    }
    console.log(`Client logon successfully`);
}
main().then();