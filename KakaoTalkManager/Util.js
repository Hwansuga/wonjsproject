"use strict";
exports.__esModule = true;
exports.UserChatInfo = exports.RoomBanWinfo = exports.wait = void 0;
function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}
exports.wait = wait;
var RoomBanWinfo = /** @class */ (function () {
    function RoomBanWinfo() {
    }
    return RoomBanWinfo;
}());
exports.RoomBanWinfo = RoomBanWinfo;
var UserChatInfo = /** @class */ (function () {
    function UserChatInfo() {
        this.arrChat = [];
    }
    return UserChatInfo;
}());
exports.UserChatInfo = UserChatInfo;
