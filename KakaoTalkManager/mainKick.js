"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var node_kakao_1 = require("node-kakao");
var appConfig = require("./AppConfig_Kick.json");
var ProcScript_1 = require("./ProcScript");
require('log-timestamp');
var RoomBanWinfo = /** @class */ (function () {
    function RoomBanWinfo() {
    }
    return RoomBanWinfo;
}());
var UserChatInfo = /** @class */ (function () {
    function UserChatInfo() {
        this.arrChat = [];
    }
    return UserChatInfo;
}());
var DEVICE_UUID = appConfig.DEVICE_UUID;
var DEVICE_NAME = appConfig.DEVICE_NAME;
var EMAIL = appConfig.EMAIL;
var PASSWORD = appConfig.PASSWORD;
var listBanWord = appConfig.listBanWord;
var Whitelist = appConfig.Whitelist;
var NickBlacklist = appConfig.NickBlacklist;
var spam = appConfig.spam;
var spamRange = appConfig.spamRange;
var spamCount = appConfig.spamCount;
var limit = appConfig.limit;
var limitRange = appConfig.limitRange;
var limitCount = appConfig.limitCount;
var CLIENT = new node_kakao_1.TalkClient();
var procScript = new ProcScript_1.ProcScript();
var listUserChatInfo = [];
function KickUser(channel, user) {
    return __awaiter(this, void 0, void 0, function () {
        var room, workKick, _a;
        var _this = this;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    room = CLIENT.channelList.open.get(channel.channelId);
                    if (room == null) {
                        return [2 /*return*/];
                    }
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 5]);
                    return [4 /*yield*/, room.kickUser(user)];
                case 2:
                    workKick = _b.sent();
                    if (workKick.success == false) {
                        procScript.AddProc(function () { return __awaiter(_this, void 0, void 0, function () {
                            var retKick, err_1;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        _a.trys.push([0, 2, , 4]);
                                        return [4 /*yield*/, room.kickUser(user)];
                                    case 1:
                                        retKick = _a.sent();
                                        if (retKick.success) {
                                            console.log("Success, Kick user " + user.nickname + " from " + room.getDisplayName());
                                        }
                                        else {
                                            console.log("Failed.., Kick user " + user.nickname + " from " + room.getDisplayName());
                                        }
                                        procScript.DoneProc();
                                        return [3 /*break*/, 4];
                                    case 2:
                                        err_1 = _a.sent();
                                        return [4 /*yield*/, Login()];
                                    case 3:
                                        _a.sent();
                                        procScript.DoneProc();
                                        return [3 /*break*/, 4];
                                    case 4: return [2 /*return*/];
                                }
                            });
                        }); }, null);
                    }
                    else {
                        console.log("Kick Success! / " + user.nickname);
                    }
                    procScript.AddProc(function () {
                        var indexTarget = listUserChatInfo.findIndex(function (x) { return x.id == user.userId; });
                        if (indexTarget >= 0) {
                            listUserChatInfo.splice(indexTarget, 1);
                        }
                        procScript.DoneProc();
                    }, null);
                    return [3 /*break*/, 5];
                case 3:
                    _a = _b.sent();
                    console.log("Err Kick!!!!");
                    return [4 /*yield*/, Login()];
                case 4:
                    _b.sent();
                    return [3 /*break*/, 5];
                case 5: return [2 /*return*/];
            }
        });
    });
}
function FilterChat(data, channel) {
    return __awaiter(this, void 0, void 0, function () {
        var sender, room, roomName, banInfo, banIndex, exIndex;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    sender = data.getSenderInfo(channel);
                    if (sender == null) {
                        return [2 /*return*/];
                    }
                    room = CLIENT.channelList.open.get(channel.channelId);
                    if (room == null) {
                        return [2 /*return*/];
                    }
                    roomName = room.getDisplayName();
                    banInfo = listBanWord[0];
                    banIndex = banInfo.banWord.findIndex(function (x) { return data.text.includes(x); });
                    if (!(banIndex >= 0)) return [3 /*break*/, 3];
                    console.log("text :  " + data.text + " / badword : " + banInfo.banWord[banIndex] + " / roomName : " + roomName + " / nickname : " + sender.nickname);
                    exIndex = Whitelist.findIndex(function (x) { return x == sender.nickname; });
                    if (!(exIndex >= 0)) return [3 /*break*/, 1];
                    console.log(sender.nickname + " pass..");
                    return [2 /*return*/];
                case 1: return [4 /*yield*/, KickUser(channel, sender)];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3: return [2 /*return*/];
            }
        });
    });
}
function CollectChatInfo(data, sender) {
    var userInfo = listUserChatInfo.find(function (x) { return x.id == sender.userId; });
    if (userInfo == null) {
        userInfo = new UserChatInfo();
        userInfo.id = sender.userId;
        userInfo.nickname = sender.nickname;
        listUserChatInfo.push(userInfo);
    }
    userInfo.arrChat.push(data);
    return true;
}
function IsSpamUser(userInfo) {
    if (spam == false) {
        return false;
    }
    if (userInfo.arrChat.length < spamCount) {
        return false;
    }
    var endIndex = userInfo.arrChat.length - 1;
    var startIndex = endIndex - spamRange;
    if (startIndex < 0) {
        startIndex = 0;
    }
    var standardChat = userInfo.arrChat[endIndex];
    var listSpamRange = userInfo.arrChat.slice(startIndex);
    var cntSpam = listSpamRange.filter(function (x) { return x.text == standardChat.text; });
    if (cntSpam.length <= spamCount) {
        return false;
    }
    console.log(standardChat.text + " \uB3C4\uBC30 \uAC80\uCD9C. / count : " + spamCount + " in " + spamRange);
    return true;
}
function IsLimitOverUser(userInfo) {
    if (limit == false) {
        return false;
    }
    if (userInfo.arrChat.length < limitCount) {
        return false;
    }
    var endIndex = userInfo.arrChat.length - 1;
    var standardChat = userInfo.arrChat[endIndex];
    var startTime = new Date(JSON.parse(JSON.stringify(standardChat.sendAt)));
    startTime.setSeconds(startTime.getSeconds() - limitRange);
    var listInRageChat = userInfo.arrChat.filter(function (x) { return x.sendAt >= startTime; });
    if (listInRageChat.length < limitCount) {
        return false;
    }
    console.log(standardChat.text + " \uC81C\uD55C\uC18D\uB3C4 \uC624\uBC84 \uAC80\uCD9C. / count : " + limitCount + " in " + limitRange + " \uCD08");
    return true;
}
CLIENT.on("chat", function (data, channel) { return __awaiter(void 0, void 0, void 0, function () {
    var sender, exIndex, userInfo;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                FilterChat(data, channel);
                sender = data.getSenderInfo(channel);
                if (sender == null) {
                    return [2 /*return*/];
                }
                exIndex = Whitelist.findIndex(function (x) { return x == sender.nickname; });
                if (exIndex >= 0) {
                    return [2 /*return*/];
                }
                if (!CollectChatInfo(data, sender)) return [3 /*break*/, 4];
                userInfo = listUserChatInfo.find(function (x) { return x.id == sender.userId; });
                if (userInfo == null) {
                    return [2 /*return*/, false];
                }
                if (!IsSpamUser(userInfo)) return [3 /*break*/, 2];
                console.log(sender.nickname + " is SpamUser!");
                return [4 /*yield*/, KickUser(channel, sender)];
            case 1:
                _a.sent();
                return [2 /*return*/];
            case 2:
                if (!IsLimitOverUser(userInfo)) return [3 /*break*/, 4];
                console.log(sender.nickname + " is LimitOverUser!");
                return [4 /*yield*/, KickUser(channel, sender)];
            case 3:
                _a.sent();
                return [2 /*return*/];
            case 4: return [2 /*return*/];
        }
    });
}); });
CLIENT.on('user_join', function (joinLog, channel, user, feed) { return __awaiter(void 0, void 0, void 0, function () {
    var exIndex, badIndex;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("User " + user.nickname + " joined channel " + channel.getDisplayName());
                exIndex = Whitelist.findIndex(function (x) { return x == user.nickname; });
                if (!(exIndex >= 0)) return [3 /*break*/, 1];
                console.log(user.nickname + " pass..");
                return [2 /*return*/];
            case 1:
                badIndex = NickBlacklist.findIndex(function (x) { return user.nickname.includes(x); });
                if (!(badIndex >= 0)) return [3 /*break*/, 3];
                return [4 /*yield*/, KickUser(channel, user)];
            case 2:
                _a.sent();
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); });
CLIENT.on('profile_changed', function (channel, lastInfo, user) { return __awaiter(void 0, void 0, void 0, function () {
    var exIndex, badIndex;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log("Profile of " + user.userId + " changed. From name: " + lastInfo.nickname);
                exIndex = Whitelist.findIndex(function (x) { return x == user.nickname; });
                if (!(exIndex >= 0)) return [3 /*break*/, 1];
                console.log(user.nickname + " pass..");
                return [2 /*return*/];
            case 1:
                badIndex = NickBlacklist.findIndex(function (x) { return user.nickname.includes(x); });
                if (!(badIndex >= 0)) return [3 /*break*/, 3];
                return [4 /*yield*/, KickUser(channel, user)];
            case 2:
                _a.sent();
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); });
CLIENT.on('chat_deleted', function (feedChatlog, channel, feed) {
    console.log(feedChatlog.text + " / " + feed.logId + " deleted by " + feedChatlog.sender.userId);
});
CLIENT.on('switch_server', function () {
    // Refresh credential and relogin client.
    Login().then(function () {
        console.log('Server switched!');
    });
});
CLIENT.on('disconnected', function (reason) {
    console.log("Disconnected!! reason: " + reason);
    Login().then(function () {
    });
});
function getLoginData() {
    return __awaiter(this, void 0, void 0, function () {
        var api, loginRes;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_kakao_1.AuthApiClient.create(DEVICE_NAME, DEVICE_UUID)];
                case 1:
                    api = _a.sent();
                    api.config.version = "3.4.2";
                    CLIENT.configuration.appVersion = '3.4.2.3187';
                    return [4 /*yield*/, api.login({
                            email: EMAIL,
                            password: PASSWORD,
                            forced: true
                        })];
                case 2:
                    loginRes = _a.sent();
                    if (!loginRes.success) {
                        throw new Error("Web login failed with status: " + loginRes.status);
                    }
                    return [2 /*return*/, loginRes.result];
            }
        });
    });
}
var session_info = '';
function Login() {
    return __awaiter(this, void 0, void 0, function () {
        var loginData, res;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getLoginData()];
                case 1:
                    loginData = _a.sent();
                    session_info = loginData.accessToken + "-" + loginData.deviceUUID;
                    return [4 /*yield*/, CLIENT.login(loginData)];
                case 2:
                    res = _a.sent();
                    if (res.success == true) {
                        console.log("KickBot Run Success!!!!");
                    }
                    else {
                        process.exit();
                    }
                    return [2 /*return*/];
            }
        });
    });
}
var Working = false;
function main() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Login()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
main().then();
