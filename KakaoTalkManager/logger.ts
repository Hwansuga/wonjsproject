import { timeStamp } from 'console';
import * as winston from 'winston';
import "winston-daily-rotate-file";

const logFormat = winston.format.printf(info => 
    `${info.timestamp} ${info.level}: ${info.message}`
);

const logger = winston.createLogger({
    //level : 'info',
    format : winston.format.combine(
        winston.format.timestamp({
            format : `YYYY-MM-DD HH:mm:ss`,
        }),
        winston.format.colorize(),
        logFormat,               
    ),  
    //defaultMeta: { service: 'user-service' },
    transports : [
        new winston.transports.Console({        
        }),
        new winston.transports.File({
            level : `info`,
            filename : `TempLog.log`
        }),
        new winston.transports.DailyRotateFile({
            level : `info`,
            datePattern : `YYYY-MM-DD_HH`,           
            dirname : `logs`,
            filename : `%DATE%.log`,
            maxFiles : 30,
            handleExceptions : true,
            zippedArchive : false,  
        }),
        new winston.transports.DailyRotateFile({
            //level : `error`,
            datePattern : `YYYY-MM-DD_HH`,
            dirname : `logs/error`,
            filename : `%DATE%.error.log`,
            maxFiles : 30,
            json : false,
            handleExceptions : true,
            zippedArchive : false
        }),         
    ],
    exitOnError : false
});

export default logger;