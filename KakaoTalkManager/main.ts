
import { AuthApiClient, ChatBuilder, KnownChatType, MentionContent, ReplyContent, TalkClient, TalkNormalChannel ,api, TalkOpenChannel, TalkChatData, TalkChannel, CommandResult, Chatlog, ChannelUser, ChannelUserInfo, Long, OpenChannelInfo, OpenChannelUserPerm} from 'node-kakao';
import * as appConfig from './AppConfig_Kick.json';
import  ProcScript  from './ProcScript'
import { wait,RoomBanWinfo,UserChatInfo } from './Util';

const listBanWord : Array<RoomBanWinfo> = appConfig.listBanWord;
const Whitelist : Array<string> = appConfig.Whitelist;

const NickBlacklist : Array<string> = appConfig.NickBlacklist;
const spamRange : number = appConfig.spamRange;
const spamCount : number = appConfig.spamCount;

const limitRange : number = appConfig.limitRange;
const limitCount : number = appConfig.limitCount;

const CLIENT = new TalkClient();
let procScript = new ProcScript();
let listUserChatInfo : Array<UserChatInfo> = [];
let listHideTargetChat : Array<Chatlog> = [];

async function KickUser(room : TalkOpenChannel , user : ChannelUserInfo) {

    try{
        let workKick = await room.kickUser(user);

        if (workKick.success == false){
            procScript.AddProc(async()=>{
                try{
                    let retKick = await room.kickUser(user);
                    if (retKick.success){
                        console.log(`Success, Kick user ${user.nickname} from ${room.getDisplayName()}`);    
                    }
                    else{
                        console.log(`Failed.., Kick user ${user.nickname} from ${room.getDisplayName()}`);
                    }
                    
                    procScript.DoneProc();
                }
                catch(err){
                    await Login();
                    procScript.DoneProc();
                }  
            } , null);   
        }
        else{
            console.log(`Kick Success! / ${user.nickname}`);
        }

        procScript.AddProc(()=>{
            let indexTarget = listUserChatInfo.findIndex(x=>x.id == user.userId);
            if (indexTarget >= 0){
                listUserChatInfo.splice(indexTarget , 1);
            }
            procScript.DoneProc();            
        } , null);
    }
    catch{
        console.log(`Err Kick!!!!`);
        await Login();
    }    
}

async function HideChat(room : TalkOpenChannel , chat : Chatlog , nickName : string , limitCnt : number = 50) {
    try{
        let cntTry = 0;
        while(true){
            console.log(`Try Hide! / ${chat.text}`);
            let workHide = await room.hideChat(chat);
            if (workHide.status == 0){
                console.log(`Hide Chat Success! / ${nickName}`);
                break;
            }

            if (cntTry > limitCnt){
                console.log(`try over cnt! / ${nickName}`);
                break;
            }

            console.log(`Hide Chat Failed! / ${nickName}`);
            wait(200);
            cntTry++;
        } 
        
        if (cntTry >= limitCnt){
            listHideTargetChat.push(chat);
        }
    }
    catch(err){
        console.log(`Err Hide!!!! : ${err.Message}`);
        await Login();
    }      
} 

async function HideUserChat(room : TalkOpenChannel , userChatInfo : UserChatInfo) {
    for(let i=0 ; i<userChatInfo.arrChat.length ; ++i){
        await HideChat(room , userChatInfo.arrChat[i].chat , userChatInfo.nickname);
    }

    procScript.AddProc(()=>{
        let indexTarget = listUserChatInfo.indexOf(userChatInfo);
        if (indexTarget >= 0){
            listUserChatInfo.splice(indexTarget , 1);
        }
        procScript.DoneProc();
    } , null);
}

async function CheckChatMsg(room : TalkOpenChannel , sender : ChannelUserInfo , data : TalkChatData) : Promise<boolean> {
    let roomName = room.getDisplayName();
    

    let banInfo = listBanWord[0];
    let targetMsg = data.chat.text;
    let targetChat = data.chat;

    console.log(`msg : ${targetMsg} , type : ${data.chat.type} , id : ${data.chat.logId}`);
    
    if (data.chat.type == KnownChatType.TEXT 
    || data.chat.type == KnownChatType.PHOTO
    || data.chat.type == KnownChatType.REPLY 
    || data.chat.type == KnownChatType.OPEN_SCHEDULE 
    || data.chat.type == KnownChatType.OPEN_POST
    || data.chat.type == KnownChatType.CUSTOM
    || data.chat.type == KnownChatType.OPEN_VOTE){
        // if (data.chat.type == KnownChatType.TEXT){
        //     console.log(`${nickname} / Chat Data : ${JSON.stringify(data)}`);
        // }

        let banIndex = banInfo.banWord.findIndex(x=> targetMsg.includes(x));
        if (banIndex > -1){
            console.log(`text :  ${targetMsg} / badword : ${banInfo.banWord[banIndex]} / roomName : ${roomName} / nickname : ${sender.nickname}`);
            await KickUser(room , sender);
            await HideChat(room , targetChat , sender.nickname);
            return true; 
        }       
    }
    else if (data.chat.type == KnownChatType.STICKERANI){
        console.log(`text :  ${targetMsg} / emoticon / roomName : ${roomName} / nickname : ${sender.nickname}`);
        await KickUser(room , sender);
        await HideChat(room , targetChat , sender.nickname);
        return true;  
    }   
    return false;
}

async function FilterChat(data : TalkChatData , channel : TalkChannel){
    const sender = data.getSenderInfo(channel);
    if (sender == null){
        return;
    }

    let room : TalkOpenChannel = CLIENT.channelList.open.get(channel.channelId);
    if (room == null){
        return;
    }
    
    let roomName = room.getDisplayName();

    //AllOne
    let banInfo = listBanWord[0];
    let banIndex = banInfo.banWord.findIndex(x=> data.text.includes(x));
    if (banIndex >= 0){
        console.log(`text :  ${data.text} / badword : ${banInfo.banWord[banIndex]} / roomName : ${roomName} / nickname : ${sender.nickname}`);
        await KickUser(room , sender);
        await HideChat(room , data.chat , sender.nickname);  
    }
}

function CollectChatInfo(data : TalkChatData , sender : ChannelUserInfo) : boolean {
    let userInfo = listUserChatInfo.find(x=>x.id == sender.userId);
    if (userInfo == null){
        userInfo = new UserChatInfo();
        userInfo.id = sender.userId;
        userInfo.nickname = sender.nickname;    
        listUserChatInfo.push(userInfo);   
    }
    userInfo.arrChat.push(data);

    return true;
}

function IsSpamUser(userInfo : UserChatInfo) : boolean{

    if (appConfig.spam == false){
        return false;
    }
    if (userInfo.arrChat.length < spamCount){
        return false;
    }

    let endIndex = userInfo.arrChat.length-1;
    let startIndex = endIndex - spamRange;
    if (startIndex < 0){
        startIndex = 0;
    }
    let standardChat = userInfo.arrChat[endIndex];
    let listSpamRange = userInfo.arrChat.slice( startIndex );
    let cntSpam = listSpamRange.filter(x=>x.text == standardChat.text);
    if (cntSpam.length <= spamCount){
        return false;
    }

    console.log(`${standardChat.text} 도배 검출. / count : ${spamCount} in ${spamRange}`);

    return true;
}

function IsLimitOverUser(userInfo : UserChatInfo) {
    if (appConfig.limit == false){
        return false;
    }

    if (userInfo.arrChat.length < limitCount){
        return false;
    }

    let endIndex = userInfo.arrChat.length-1;

    let standardChat = userInfo.arrChat[endIndex];
    let startTime = new Date( JSON.parse(JSON.stringify(standardChat.sendAt)) );
    startTime.setSeconds(startTime.getSeconds() - limitRange);
    let listInRageChat = userInfo.arrChat.filter(x=>x.sendAt >= startTime);
    if (listInRageChat.length < limitCount){
        return false;
    }

    console.log(`${standardChat.text} 제한속도 오버 검출. / count : ${limitCount} in ${limitRange} 초`);

    return true;
}

CLIENT.on(`chat` , async (data , channel)=>{
    let room : TalkOpenChannel = CLIENT.channelList.open.get(channel.channelId);
    if (room == null){
        return;
    }

    let sender = data.getSenderInfo(channel);
    if (sender == null){
        return;
    }
    
    let openUerInfo = room.getUserInfo(sender);
    console.log(`${openUerInfo.nickname} Perm : ${openUerInfo.perm}`);
    //매니져나 방장이면 패스
    if (openUerInfo.perm != OpenChannelUserPerm.NONE){
        return;
    }

    let exIndex = Whitelist.findIndex(x=>x == sender.nickname);
    if (exIndex >= 0){
        return;
    } 

    //FilterChat(data , channel);
 
    let retCheckMsg = await CheckChatMsg(room , sender , data);
    if (retCheckMsg == true){
        return;
    }

    if(CollectChatInfo(data , sender)){        
        let userInfo = listUserChatInfo.find(x=>x.id == sender.userId);
        if (userInfo == null){
            return false;
        }         

        if (IsSpamUser(userInfo)){
            console.log(`${sender.nickname} is SpamUser!`);
            await KickUser(room , sender);
            await HideUserChat(room , userInfo);
            return;
        }

        if (IsLimitOverUser(userInfo)){
            console.log(`${sender.nickname} is LimitOverUser!`);
            await KickUser(room , sender);
            await HideUserChat(room , userInfo);
            return;
        }
    }
});

CLIENT.on('user_join', async (joinLog, channel, user, feed) => {
    console.log(`User ${user.nickname} joined channel ${channel.getDisplayName()}`);

    let room : TalkOpenChannel = CLIENT.channelList.open.get(channel.channelId);
    if (room == null){
        return;
    }

    let exIndex = Whitelist.findIndex(x=>x == user.nickname);
    if (exIndex >= 0){
        console.log(`${user.nickname} pass..`);
        return;
    }
    else{
        let badIndex = NickBlacklist.findIndex(x=> user.nickname.includes(x));
        if (badIndex >= 0){
            await KickUser(room , user);
        }
        
    }
});

CLIENT.on('profile_changed',async (channel, lastInfo, user) => {
    console.log(`Profile of ${user.userId} changed. From name: ${lastInfo.nickname}`);

    let room : TalkOpenChannel = CLIENT.channelList.open.get(channel.channelId);
    if (room == null){
        return;
    }

    let exIndex = Whitelist.findIndex(x=>x == user.nickname);
    if (exIndex >= 0){
        console.log(`${user.nickname} pass..`);
        return;
    }
    else{
        let badIndex = NickBlacklist.findIndex(x=> user.nickname.includes(x));
        if (badIndex >= 0){
            await KickUser(room , user);
        }
    }
});

CLIENT.on('message_hidden', (hideLog, channel, feed) => {
    console.log(`Message ${hideLog.text} hid from ${channel.getDisplayName()} by ${hideLog.sender.userId}`);
});


CLIENT.on('chat_deleted', (feedChatlog, channel, feed) => {
    console.log(`${feedChatlog.text} / ${feed.logId} deleted by ${feedChatlog.sender.userId}`);
});

CLIENT.on('switch_server', () => {
    // Refresh credential and relogin client.
    Login().then(() => {
      console.log('Server switched!');
    });
});

CLIENT.on('disconnected', (reason) => {
    console.log(`Disconnected!! reason: ${reason}`);
    Login().then(()=>{

    });
});

async function getLoginData(): Promise<api.LoginData> {
    const api = await AuthApiClient.create(appConfig.DEVICE_NAME, appConfig.DEVICE_UUID);
    api.config.version = `3.4.2`;
    CLIENT.configuration.appVersion = '3.4.2.3187';
    const loginRes = await api.login({
      email: appConfig.EMAIL,
      password: appConfig.PASSWORD,
      forced: true,
    });

    if (!loginRes.success){
        throw new Error(`Web login failed with status: ${loginRes.status}`);
    } 
  
    return loginRes.result;
}

let session_info : string = '';
async function Login() {
    const loginData = await getLoginData();
    session_info = `${loginData.accessToken}-${loginData.deviceUUID}`;
    const res = await CLIENT.login(loginData);

    if (res.success == true){
        console.log(`MainBot Run Success!!!!`);
    }
    else{
        process.exit();
    }  
}

let Working : Boolean = false;
async function  main() {
    await Login();
}
main().then();