﻿
namespace ConfigManager
{
    partial class Form_Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_Save = new System.Windows.Forms.Button();
            this.button_DeviceUID = new System.Windows.Forms.Button();
            this.button_DeviceName = new System.Windows.Forms.Button();
            this.comboBox_Item = new System.Windows.Forms.ComboBox();
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.dataGridView_Whitelist = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_DelWhite = new System.Windows.Forms.Button();
            this.button_AddWhite = new System.Windows.Forms.Button();
            this.textBox_White = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Id = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Pw = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_DeviceName = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_DeviceUid = new System.Windows.Forms.TextBox();
            this.tabControl_RoomController = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.roomInfoController1 = new ConfigManager.RoomInfoController();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.roomInfoController2 = new ConfigManager.RoomInfoController();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.roomInfoController3 = new ConfigManager.RoomInfoController();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.roomInfoController4 = new ConfigManager.RoomInfoController();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.roomInfoController5 = new ConfigManager.RoomInfoController();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.roomInfoController6 = new ConfigManager.RoomInfoController();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.roomInfoController7 = new ConfigManager.RoomInfoController();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.roomInfoController8 = new ConfigManager.RoomInfoController();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.roomInfoController9 = new ConfigManager.RoomInfoController();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.roomInfoController10 = new ConfigManager.RoomInfoController();
            this.tableLayoutPanel_Spam = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox_Spam = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel_SpamDetail = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_SpamRange = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_SpamCount = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel_Limit = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox_Limit = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel_LimitDetail = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown_LimitRange = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_LimitCount = new System.Windows.Forms.NumericUpDown();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button_DelNickBlack = new System.Windows.Forms.Button();
            this.button_AddNickBlack = new System.Windows.Forms.Button();
            this.textBox_NickBlack = new System.Windows.Forms.TextBox();
            this.dataGridView_NickBlackList = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDown_CheckMinExit = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Whitelist)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabControl_RoomController.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tableLayoutPanel_Spam.SuspendLayout();
            this.tableLayoutPanel_SpamDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SpamRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SpamCount)).BeginInit();
            this.tableLayoutPanel_Limit.SuspendLayout();
            this.tableLayoutPanel_LimitDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitCount)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_NickBlackList)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CheckMinExit)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.button_Save);
            this.panel1.Controls.Add(this.button_DeviceUID);
            this.panel1.Controls.Add(this.button_DeviceName);
            this.panel1.Controls.Add(this.comboBox_Item);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(134, 413);
            this.panel1.TabIndex = 0;
            // 
            // button_Save
            // 
            this.button_Save.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_Save.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Save.Location = new System.Drawing.Point(0, 69);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(134, 23);
            this.button_Save.TabIndex = 3;
            this.button_Save.Text = "저장";
            this.button_Save.UseVisualStyleBackColor = false;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // button_DeviceUID
            // 
            this.button_DeviceUID.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_DeviceUID.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_DeviceUID.Location = new System.Drawing.Point(0, 46);
            this.button_DeviceUID.Name = "button_DeviceUID";
            this.button_DeviceUID.Size = new System.Drawing.Size(134, 23);
            this.button_DeviceUID.TabIndex = 1;
            this.button_DeviceUID.Text = "장치UID 랜덤 생성";
            this.button_DeviceUID.UseVisualStyleBackColor = false;
            this.button_DeviceUID.Click += new System.EventHandler(this.button_DeviceUID_Click);
            // 
            // button_DeviceName
            // 
            this.button_DeviceName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button_DeviceName.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_DeviceName.Location = new System.Drawing.Point(0, 23);
            this.button_DeviceName.Name = "button_DeviceName";
            this.button_DeviceName.Size = new System.Drawing.Size(134, 23);
            this.button_DeviceName.TabIndex = 0;
            this.button_DeviceName.Text = "장치이름 랜덤 생성";
            this.button_DeviceName.UseVisualStyleBackColor = false;
            this.button_DeviceName.Click += new System.EventHandler(this.button_DeviceName_Click);
            // 
            // comboBox_Item
            // 
            this.comboBox_Item.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox_Item.FormattingEnabled = true;
            this.comboBox_Item.Items.AddRange(new object[] {
            "Hide",
            "Kick",
            "Check"});
            this.comboBox_Item.Location = new System.Drawing.Point(0, 0);
            this.comboBox_Item.Name = "comboBox_Item";
            this.comboBox_Item.Size = new System.Drawing.Size(134, 23);
            this.comboBox_Item.TabIndex = 2;
            this.comboBox_Item.SelectedIndexChanged += new System.EventHandler(this.comboBox_Item_SelectedIndexChanged);
            // 
            // listBox_Log
            // 
            this.listBox_Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 15;
            this.listBox_Log.Location = new System.Drawing.Point(0, 413);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(963, 169);
            this.listBox_Log.TabIndex = 3;
            // 
            // dataGridView_Whitelist
            // 
            this.dataGridView_Whitelist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_Whitelist.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_Whitelist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Whitelist.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView_Whitelist.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView_Whitelist.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_Whitelist.MultiSelect = false;
            this.dataGridView_Whitelist.Name = "dataGridView_Whitelist";
            this.dataGridView_Whitelist.ReadOnly = true;
            this.dataGridView_Whitelist.RowHeadersVisible = false;
            this.dataGridView_Whitelist.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dataGridView_Whitelist.RowTemplate.Height = 25;
            this.dataGridView_Whitelist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_Whitelist.Size = new System.Drawing.Size(176, 303);
            this.dataGridView_Whitelist.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_DelWhite);
            this.panel2.Controls.Add(this.button_AddWhite);
            this.panel2.Controls.Add(this.textBox_White);
            this.panel2.Controls.Add(this.dataGridView_Whitelist);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(134, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(176, 413);
            this.panel2.TabIndex = 4;
            // 
            // button_DelWhite
            // 
            this.button_DelWhite.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_DelWhite.Location = new System.Drawing.Point(0, 349);
            this.button_DelWhite.Name = "button_DelWhite";
            this.button_DelWhite.Size = new System.Drawing.Size(176, 23);
            this.button_DelWhite.TabIndex = 4;
            this.button_DelWhite.Text = "삭제";
            this.button_DelWhite.UseVisualStyleBackColor = true;
            this.button_DelWhite.Click += new System.EventHandler(this.button_DelWhite_Click);
            // 
            // button_AddWhite
            // 
            this.button_AddWhite.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_AddWhite.Location = new System.Drawing.Point(0, 326);
            this.button_AddWhite.Name = "button_AddWhite";
            this.button_AddWhite.Size = new System.Drawing.Size(176, 23);
            this.button_AddWhite.TabIndex = 3;
            this.button_AddWhite.Text = "추가";
            this.button_AddWhite.UseVisualStyleBackColor = true;
            this.button_AddWhite.Click += new System.EventHandler(this.button_AddWhite_Click);
            // 
            // textBox_White
            // 
            this.textBox_White.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox_White.Location = new System.Drawing.Point(0, 303);
            this.textBox_White.Name = "textBox_White";
            this.textBox_White.Size = new System.Drawing.Size(176, 23);
            this.textBox_White.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_Id, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(751, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 57);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "카카오톡 아이디";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_Id
            // 
            this.textBox_Id.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox_Id.Location = new System.Drawing.Point(4, 32);
            this.textBox_Id.Name = "textBox_Id";
            this.textBox_Id.Size = new System.Drawing.Size(192, 23);
            this.textBox_Id.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBox_Pw, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(751, 63);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 57);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 27);
            this.label2.TabIndex = 0;
            this.label2.Text = "카카오톡 비번";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_Pw
            // 
            this.textBox_Pw.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox_Pw.Location = new System.Drawing.Point(4, 32);
            this.textBox_Pw.Name = "textBox_Pw";
            this.textBox_Pw.Size = new System.Drawing.Size(192, 23);
            this.textBox_Pw.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBox_DeviceName, 0, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(751, 123);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(200, 57);
            this.tableLayoutPanel3.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(192, 27);
            this.label3.TabIndex = 0;
            this.label3.Text = "장치이름";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_DeviceName
            // 
            this.textBox_DeviceName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox_DeviceName.Location = new System.Drawing.Point(4, 32);
            this.textBox_DeviceName.Name = "textBox_DeviceName";
            this.textBox_DeviceName.Size = new System.Drawing.Size(192, 23);
            this.textBox_DeviceName.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBox_DeviceUid, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(751, 186);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(200, 57);
            this.tableLayoutPanel4.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(4, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(192, 27);
            this.label4.TabIndex = 0;
            this.label4.Text = "장치 UID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_DeviceUid
            // 
            this.textBox_DeviceUid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox_DeviceUid.Location = new System.Drawing.Point(4, 32);
            this.textBox_DeviceUid.Name = "textBox_DeviceUid";
            this.textBox_DeviceUid.Size = new System.Drawing.Size(192, 23);
            this.textBox_DeviceUid.TabIndex = 1;
            // 
            // tabControl_RoomController
            // 
            this.tabControl_RoomController.Controls.Add(this.tabPage1);
            this.tabControl_RoomController.Controls.Add(this.tabPage2);
            this.tabControl_RoomController.Controls.Add(this.tabPage3);
            this.tabControl_RoomController.Controls.Add(this.tabPage4);
            this.tabControl_RoomController.Controls.Add(this.tabPage5);
            this.tabControl_RoomController.Controls.Add(this.tabPage6);
            this.tabControl_RoomController.Controls.Add(this.tabPage7);
            this.tabControl_RoomController.Controls.Add(this.tabPage8);
            this.tabControl_RoomController.Controls.Add(this.tabPage9);
            this.tabControl_RoomController.Controls.Add(this.tabPage10);
            this.tabControl_RoomController.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl_RoomController.Location = new System.Drawing.Point(310, 0);
            this.tabControl_RoomController.Name = "tabControl_RoomController";
            this.tabControl_RoomController.SelectedIndex = 0;
            this.tabControl_RoomController.Size = new System.Drawing.Size(252, 413);
            this.tabControl_RoomController.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.roomInfoController1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(244, 385);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "방1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // roomInfoController1
            // 
            this.roomInfoController1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roomInfoController1.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController1.Name = "roomInfoController1";
            this.roomInfoController1.Size = new System.Drawing.Size(238, 379);
            this.roomInfoController1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.roomInfoController2);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(244, 385);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "방2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // roomInfoController2
            // 
            this.roomInfoController2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roomInfoController2.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController2.Name = "roomInfoController2";
            this.roomInfoController2.Size = new System.Drawing.Size(238, 379);
            this.roomInfoController2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.roomInfoController3);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(244, 385);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "방3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // roomInfoController3
            // 
            this.roomInfoController3.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController3.Name = "roomInfoController3";
            this.roomInfoController3.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController3.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.roomInfoController4);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(244, 385);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "방4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // roomInfoController4
            // 
            this.roomInfoController4.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController4.Name = "roomInfoController4";
            this.roomInfoController4.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController4.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.roomInfoController5);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(244, 385);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "방5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // roomInfoController5
            // 
            this.roomInfoController5.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController5.Name = "roomInfoController5";
            this.roomInfoController5.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController5.TabIndex = 1;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.roomInfoController6);
            this.tabPage6.Location = new System.Drawing.Point(4, 24);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(244, 385);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "방6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // roomInfoController6
            // 
            this.roomInfoController6.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController6.Name = "roomInfoController6";
            this.roomInfoController6.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController6.TabIndex = 2;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.roomInfoController7);
            this.tabPage7.Location = new System.Drawing.Point(4, 24);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(244, 385);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "방7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // roomInfoController7
            // 
            this.roomInfoController7.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController7.Name = "roomInfoController7";
            this.roomInfoController7.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController7.TabIndex = 2;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.roomInfoController8);
            this.tabPage8.Location = new System.Drawing.Point(4, 24);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(244, 385);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "방8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // roomInfoController8
            // 
            this.roomInfoController8.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController8.Name = "roomInfoController8";
            this.roomInfoController8.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController8.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.roomInfoController9);
            this.tabPage9.Location = new System.Drawing.Point(4, 24);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(244, 385);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "방9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // roomInfoController9
            // 
            this.roomInfoController9.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController9.Name = "roomInfoController9";
            this.roomInfoController9.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController9.TabIndex = 2;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.roomInfoController10);
            this.tabPage10.Location = new System.Drawing.Point(4, 24);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(244, 385);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "방10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // roomInfoController10
            // 
            this.roomInfoController10.Location = new System.Drawing.Point(3, 3);
            this.roomInfoController10.Name = "roomInfoController10";
            this.roomInfoController10.Size = new System.Drawing.Size(238, 340);
            this.roomInfoController10.TabIndex = 2;
            // 
            // tableLayoutPanel_Spam
            // 
            this.tableLayoutPanel_Spam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel_Spam.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel_Spam.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel_Spam.ColumnCount = 2;
            this.tableLayoutPanel_Spam.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel_Spam.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel_Spam.Controls.Add(this.checkBox_Spam, 0, 0);
            this.tableLayoutPanel_Spam.Controls.Add(this.tableLayoutPanel_SpamDetail, 1, 0);
            this.tableLayoutPanel_Spam.Location = new System.Drawing.Point(751, 249);
            this.tableLayoutPanel_Spam.Name = "tableLayoutPanel_Spam";
            this.tableLayoutPanel_Spam.RowCount = 1;
            this.tableLayoutPanel_Spam.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Spam.Size = new System.Drawing.Size(200, 58);
            this.tableLayoutPanel_Spam.TabIndex = 11;
            // 
            // checkBox_Spam
            // 
            this.checkBox_Spam.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Spam.AutoSize = true;
            this.checkBox_Spam.Checked = true;
            this.checkBox_Spam.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_Spam.Location = new System.Drawing.Point(4, 4);
            this.checkBox_Spam.Name = "checkBox_Spam";
            this.checkBox_Spam.Size = new System.Drawing.Size(59, 50);
            this.checkBox_Spam.TabIndex = 0;
            this.checkBox_Spam.Text = "Spam";
            this.checkBox_Spam.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel_SpamDetail
            // 
            this.tableLayoutPanel_SpamDetail.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel_SpamDetail.ColumnCount = 2;
            this.tableLayoutPanel_SpamDetail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_SpamDetail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_SpamDetail.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel_SpamDetail.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel_SpamDetail.Controls.Add(this.numericUpDown_SpamRange, 0, 1);
            this.tableLayoutPanel_SpamDetail.Controls.Add(this.numericUpDown_SpamCount, 1, 1);
            this.tableLayoutPanel_SpamDetail.Location = new System.Drawing.Point(70, 4);
            this.tableLayoutPanel_SpamDetail.Name = "tableLayoutPanel_SpamDetail";
            this.tableLayoutPanel_SpamDetail.RowCount = 2;
            this.tableLayoutPanel_SpamDetail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_SpamDetail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_SpamDetail.Size = new System.Drawing.Size(126, 50);
            this.tableLayoutPanel_SpamDetail.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(4, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 23);
            this.label5.TabIndex = 1;
            this.label5.Text = "Range";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(66, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Count";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_SpamRange
            // 
            this.numericUpDown_SpamRange.Location = new System.Drawing.Point(4, 28);
            this.numericUpDown_SpamRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_SpamRange.Name = "numericUpDown_SpamRange";
            this.numericUpDown_SpamRange.Size = new System.Drawing.Size(55, 23);
            this.numericUpDown_SpamRange.TabIndex = 3;
            this.numericUpDown_SpamRange.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // numericUpDown_SpamCount
            // 
            this.numericUpDown_SpamCount.Location = new System.Drawing.Point(66, 28);
            this.numericUpDown_SpamCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_SpamCount.Name = "numericUpDown_SpamCount";
            this.numericUpDown_SpamCount.Size = new System.Drawing.Size(56, 23);
            this.numericUpDown_SpamCount.TabIndex = 4;
            this.numericUpDown_SpamCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // tableLayoutPanel_Limit
            // 
            this.tableLayoutPanel_Limit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel_Limit.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel_Limit.ColumnCount = 2;
            this.tableLayoutPanel_Limit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel_Limit.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel_Limit.Controls.Add(this.checkBox_Limit, 0, 0);
            this.tableLayoutPanel_Limit.Controls.Add(this.tableLayoutPanel_LimitDetail, 1, 0);
            this.tableLayoutPanel_Limit.Location = new System.Drawing.Point(751, 310);
            this.tableLayoutPanel_Limit.Name = "tableLayoutPanel_Limit";
            this.tableLayoutPanel_Limit.RowCount = 1;
            this.tableLayoutPanel_Limit.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Limit.Size = new System.Drawing.Size(200, 58);
            this.tableLayoutPanel_Limit.TabIndex = 12;
            // 
            // checkBox_Limit
            // 
            this.checkBox_Limit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_Limit.AutoSize = true;
            this.checkBox_Limit.Checked = true;
            this.checkBox_Limit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_Limit.Location = new System.Drawing.Point(4, 4);
            this.checkBox_Limit.Name = "checkBox_Limit";
            this.checkBox_Limit.Size = new System.Drawing.Size(59, 50);
            this.checkBox_Limit.TabIndex = 0;
            this.checkBox_Limit.Text = "Limit";
            this.checkBox_Limit.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel_LimitDetail
            // 
            this.tableLayoutPanel_LimitDetail.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel_LimitDetail.ColumnCount = 2;
            this.tableLayoutPanel_LimitDetail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_LimitDetail.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_LimitDetail.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel_LimitDetail.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel_LimitDetail.Controls.Add(this.numericUpDown_LimitRange, 0, 1);
            this.tableLayoutPanel_LimitDetail.Controls.Add(this.numericUpDown_LimitCount, 1, 1);
            this.tableLayoutPanel_LimitDetail.Location = new System.Drawing.Point(70, 4);
            this.tableLayoutPanel_LimitDetail.Name = "tableLayoutPanel_LimitDetail";
            this.tableLayoutPanel_LimitDetail.RowCount = 2;
            this.tableLayoutPanel_LimitDetail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_LimitDetail.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_LimitDetail.Size = new System.Drawing.Size(126, 50);
            this.tableLayoutPanel_LimitDetail.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(4, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 23);
            this.label7.TabIndex = 1;
            this.label7.Text = "Range";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(66, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 23);
            this.label8.TabIndex = 2;
            this.label8.Text = "Count";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_LimitRange
            // 
            this.numericUpDown_LimitRange.Location = new System.Drawing.Point(4, 28);
            this.numericUpDown_LimitRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_LimitRange.Name = "numericUpDown_LimitRange";
            this.numericUpDown_LimitRange.Size = new System.Drawing.Size(55, 23);
            this.numericUpDown_LimitRange.TabIndex = 3;
            this.numericUpDown_LimitRange.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // numericUpDown_LimitCount
            // 
            this.numericUpDown_LimitCount.Location = new System.Drawing.Point(66, 28);
            this.numericUpDown_LimitCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_LimitCount.Name = "numericUpDown_LimitCount";
            this.numericUpDown_LimitCount.Size = new System.Drawing.Size(56, 23);
            this.numericUpDown_LimitCount.TabIndex = 4;
            this.numericUpDown_LimitCount.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button_DelNickBlack);
            this.panel3.Controls.Add(this.button_AddNickBlack);
            this.panel3.Controls.Add(this.textBox_NickBlack);
            this.panel3.Controls.Add(this.dataGridView_NickBlackList);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(562, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(176, 413);
            this.panel3.TabIndex = 13;
            // 
            // button_DelNickBlack
            // 
            this.button_DelNickBlack.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_DelNickBlack.Location = new System.Drawing.Point(0, 349);
            this.button_DelNickBlack.Name = "button_DelNickBlack";
            this.button_DelNickBlack.Size = new System.Drawing.Size(176, 23);
            this.button_DelNickBlack.TabIndex = 4;
            this.button_DelNickBlack.Text = "삭제";
            this.button_DelNickBlack.UseVisualStyleBackColor = true;
            this.button_DelNickBlack.Click += new System.EventHandler(this.button_DelNickBlack_Click);
            // 
            // button_AddNickBlack
            // 
            this.button_AddNickBlack.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_AddNickBlack.Location = new System.Drawing.Point(0, 326);
            this.button_AddNickBlack.Name = "button_AddNickBlack";
            this.button_AddNickBlack.Size = new System.Drawing.Size(176, 23);
            this.button_AddNickBlack.TabIndex = 3;
            this.button_AddNickBlack.Text = "추가";
            this.button_AddNickBlack.UseVisualStyleBackColor = true;
            this.button_AddNickBlack.Click += new System.EventHandler(this.button_AddNickBlack_Click);
            // 
            // textBox_NickBlack
            // 
            this.textBox_NickBlack.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox_NickBlack.Location = new System.Drawing.Point(0, 303);
            this.textBox_NickBlack.Name = "textBox_NickBlack";
            this.textBox_NickBlack.Size = new System.Drawing.Size(176, 23);
            this.textBox_NickBlack.TabIndex = 2;
            // 
            // dataGridView_NickBlackList
            // 
            this.dataGridView_NickBlackList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_NickBlackList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_NickBlackList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_NickBlackList.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView_NickBlackList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView_NickBlackList.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_NickBlackList.MultiSelect = false;
            this.dataGridView_NickBlackList.Name = "dataGridView_NickBlackList";
            this.dataGridView_NickBlackList.ReadOnly = true;
            this.dataGridView_NickBlackList.RowHeadersVisible = false;
            this.dataGridView_NickBlackList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dataGridView_NickBlackList.RowTemplate.Height = 25;
            this.dataGridView_NickBlackList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_NickBlackList.Size = new System.Drawing.Size(176, 303);
            this.dataGridView_NickBlackList.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.5F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.5F));
            this.tableLayoutPanel5.Controls.Add(this.numericUpDown_CheckMinExit, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(751, 374);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(200, 32);
            this.tableLayoutPanel5.TabIndex = 14;
            this.tableLayoutPanel5.Visible = false;
            // 
            // numericUpDown_CheckMinExit
            // 
            this.numericUpDown_CheckMinExit.Location = new System.Drawing.Point(138, 3);
            this.numericUpDown_CheckMinExit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_CheckMinExit.Name = "numericUpDown_CheckMinExit";
            this.numericUpDown_CheckMinExit.Size = new System.Drawing.Size(59, 23);
            this.numericUpDown_CheckMinExit.TabIndex = 4;
            this.numericUpDown_CheckMinExit.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 32);
            this.label9.TabIndex = 1;
            this.label9.Text = "종료 감지 시간(분)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 582);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tableLayoutPanel_Limit);
            this.Controls.Add(this.tableLayoutPanel_Spam);
            this.Controls.Add(this.tabControl_RoomController);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listBox_Log);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "ConfigManager";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Whitelist)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabControl_RoomController.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tableLayoutPanel_Spam.ResumeLayout(false);
            this.tableLayoutPanel_Spam.PerformLayout();
            this.tableLayoutPanel_SpamDetail.ResumeLayout(false);
            this.tableLayoutPanel_SpamDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SpamRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_SpamCount)).EndInit();
            this.tableLayoutPanel_Limit.ResumeLayout(false);
            this.tableLayoutPanel_Limit.PerformLayout();
            this.tableLayoutPanel_LimitDetail.ResumeLayout(false);
            this.tableLayoutPanel_LimitDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitCount)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_NickBlackList)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CheckMinExit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_DeviceUID;
        private System.Windows.Forms.Button button_DeviceName;
        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.ComboBox comboBox_Item;
        private System.Windows.Forms.DataGridView dataGridView_Whitelist;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_DelWhite;
        private System.Windows.Forms.Button button_AddWhite;
        private System.Windows.Forms.TextBox textBox_White;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Id;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Pw;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_DeviceName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_DeviceUid;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.TabControl tabControl_RoomController;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private RoomInfoController roomInfoController1;
        private RoomInfoController roomInfoController2;
        private RoomInfoController roomInfoController3;
        private RoomInfoController roomInfoController4;
        private RoomInfoController roomInfoController5;
        private System.Windows.Forms.TabPage tabPage6;
        private RoomInfoController roomInfoController6;
        private System.Windows.Forms.TabPage tabPage7;
        private RoomInfoController roomInfoController7;
        private System.Windows.Forms.TabPage tabPage8;
        private RoomInfoController roomInfoController8;
        private System.Windows.Forms.TabPage tabPage9;
        private RoomInfoController roomInfoController9;
        private System.Windows.Forms.TabPage tabPage10;
        private RoomInfoController roomInfoController10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Spam;
        private System.Windows.Forms.CheckBox checkBox_Spam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_SpamDetail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_SpamRange;
        private System.Windows.Forms.NumericUpDown numericUpDown_SpamCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Limit;
        private System.Windows.Forms.CheckBox checkBox_Limit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_LimitDetail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown_LimitRange;
        private System.Windows.Forms.NumericUpDown numericUpDown_LimitCount;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button_DelNickBlack;
        private System.Windows.Forms.Button button_AddNickBlack;
        private System.Windows.Forms.TextBox textBox_NickBlack;
        private System.Windows.Forms.DataGridView dataGridView_NickBlackList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.NumericUpDown numericUpDown_CheckMinExit;
        private System.Windows.Forms.Label label9;
    }
}

