﻿using ConfigManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Config
{
    public string DEVICE_NAME { get; set; } = "";
    public string DEVICE_UUID { get; set; } = "";
    public string EMAIL { get; set; } = "";
    public string PASSWORD { get; set; } = "";

    public IEnumerable<RoomBaninfo> listBanWord { get; set; } = Enumerable.Empty<RoomBaninfo>();
    //public List<RoomBaninfo> listBanWord { get; set; } = new List<RoomBaninfo>();
    public List<string> Whitelist { get; set; } = new List<string>();
    public List<string> NickBlacklist { get; set; } = new List<string>();

    public bool spam { get; set; } = true;
    public int spamRange { get; set; } = 30;
    public int spamCount { get; set; } = 5;
    public bool limit { get; set; } = true;
    public int limitRange { get; set; } = 3;
    public int limitCount { get; set; } = 6;

    public int minCheckExit { get; set; } = 5;
}

public class RoomBaninfo
{
    public string roomName { get; set; } = "";
    public List<string> banWord { get; set; } = new List<string>();
}


class Global : Singleton<Global>
{
    public static Form_Main uiController = null;

    public static void PrintLog(string msg_)
    {
        if (uiController == null)
            return;

        uiController.PrintLog(msg_);
    }
}

