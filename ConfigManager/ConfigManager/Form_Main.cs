﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigManager
{
    public partial class Form_Main : Form
    {
        readonly object thisLock = new object();

        public Form_Main()
        {
            InitializeComponent();
        }

        public void PrintLog(string msg_)
        {
            lock (thisLock)
            {
                listBox_Log.Invoke(new MethodInvoker(delegate ()
                {
                    Util.AutoLineStringAdd(listBox_Log, msg_);
                }));
            }
        }

        private void button_DeviceName_Click(object sender, EventArgs e)
        {
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            int number = rand.Next(100, 10000000);

            string name = $"Desktop{number}";
            PrintLog($"생성된 장치 이름 : {name}");
            textBox_DeviceName.Text = name;
        }

        void SetOneRoom()
        {
            for (int i = 2; i <= 10; ++i)
            {
                tabControl_RoomController.Controls.RemoveByKey($"tabPage{i}");
            }
            tabControl_RoomController.TabPages[0].Text = "금칙어";
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            SetOneRoom();

#if DEBUG
            comboBox_Item.Items.Add("TEST");
            comboBox_Item.Items.Add("TEST2");
#endif
            comboBox_Item.SelectedIndex = 0;
                     
        }

        void UpdateUI(Config config)
        {
            textBox_Id.Text = config.EMAIL;
            textBox_Pw.Text = config.PASSWORD;
            textBox_DeviceUid.Text = config.DEVICE_UUID;
            textBox_DeviceName.Text = config.DEVICE_NAME;


            dataGridView_Whitelist.DataSource = null;
            dataGridView_Whitelist.DataSource = config.Whitelist.Select(p => new { 예외아이디 = p }).ToList();
            dataGridView_Whitelist.Refresh();

            dataGridView_NickBlackList.DataSource = null;
            dataGridView_NickBlackList.DataSource = config.NickBlacklist.Select(p => new { 추방아이디 = p }).ToList();
            dataGridView_NickBlackList.Refresh();


            var allProperty = GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            var listFieldRoom = allProperty.FindAll(x => x.FieldType.Name == typeof(RoomInfoController).Name);
            var listRoomInfoController = listFieldRoom.ConvertAll(x=>(RoomInfoController)x.GetValue(this));

            for (int i=0; i < listRoomInfoController.Count; ++i)
            {
                if (config.listBanWord.Count()-1 < i)
                {
                    config.listBanWord = config.listBanWord.Append(new RoomBaninfo());
                }
                listRoomInfoController[i].SetInfo(config.listBanWord.ToList().ElementAt(i));
            }

            checkBox_Spam.Checked = config.spam;
            numericUpDown_SpamRange.Value = config.spamRange;
            numericUpDown_SpamCount.Value = config.spamCount;
            checkBox_Limit.Checked = config.limit;
            numericUpDown_LimitRange.Value = config.limitRange;
            numericUpDown_LimitCount.Value = config.limitCount;

            numericUpDown_CheckMinExit.Value = config.minCheckExit;
        }

        Config selectedConfig = new Config();
        private void comboBox_Item_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = comboBox_Item.SelectedItem.ToString();
            var fileName = $"AppConfig_{item}.json";
            if (File.Exists(fileName))
            {
                PrintLog($"{item} 파일 오픈");
                var file = File.ReadAllText(fileName);
                selectedConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<Config>(file);
                UpdateUI(selectedConfig);
            }
            else
            {
                PrintLog($"{item} 해당 관련 파일은 빈값입니다.");
                selectedConfig = new Config();
                UpdateUI(selectedConfig);
            }    
        }

        private void button_AddWhite_Click(object sender, EventArgs e)
        {
            var addValeu = textBox_White.Text.Trim();
            if (string.IsNullOrEmpty(addValeu))
                return;

            PrintLog($"{addValeu} 예외 아이디에 추가");

            selectedConfig.Whitelist.Add(addValeu);
            dataGridView_Whitelist.DataSource = null;
            dataGridView_Whitelist.DataSource = selectedConfig.Whitelist.Select(p => new { 예외아이디 = p }).ToList();
            dataGridView_Whitelist.Refresh();
        }

        private void button_DelWhite_Click(object sender, EventArgs e)
        {
            if (dataGridView_Whitelist.CurrentCell == null)
                return;

            int selectedIndex = dataGridView_Whitelist.CurrentCell.RowIndex;
            string target = selectedConfig.Whitelist.ElementAt(selectedIndex);
            PrintLog($"{target} 예외 아이디 삭제");
            selectedConfig.Whitelist.RemoveAt(selectedIndex);

            dataGridView_Whitelist.DataSource = null;
            dataGridView_Whitelist.DataSource = selectedConfig.Whitelist.Select(p => new { 예외아이디 = p }).ToList();
            dataGridView_Whitelist.Refresh();
        }

        private void button_DeviceUID_Click(object sender, EventArgs e)
        {
            ProcessStartInfo cmd = new ProcessStartInfo();
            Process process = new Process();
            cmd.FileName = @"cmd";
            cmd.WindowStyle = ProcessWindowStyle.Hidden;             // cmd창이 숨겨지도록 하기
            cmd.CreateNoWindow = true;

            cmd.UseShellExecute = false;
            cmd.RedirectStandardOutput = true;        // cmd창에서 데이터를 가져오기
            cmd.RedirectStandardInput = true;          // cmd창으로 데이터 보내기
            cmd.RedirectStandardError = true;          // cmd창에서 오류 내용 가져오기

            process.EnableRaisingEvents = false;
            process.StartInfo = cmd;
            process.Start();

            process.StandardInput.Write($"node getDeviceUUID.js" + Environment.NewLine);
            // 명령어를 보낼때는 꼭 마무리를 해줘야 한다. 그래서 마지막에 NewLine가 필요하다
            process.StandardInput.Close();

            string result = process.StandardOutput.ReadToEnd();

            string uid = Util.GetSubstringByString("getDeviceUUID.js", "==", result);

            uid = uid.Trim() + "==";

            PrintLog($"UID 생성 : {uid}");
            process.WaitForExit();
            process.Close();

            textBox_DeviceUid.Text = uid;
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            var item = comboBox_Item.SelectedItem.ToString();
            var fileName = $"AppConfig_{item}.json";

            selectedConfig.DEVICE_NAME = textBox_DeviceName.Text;
            selectedConfig.DEVICE_UUID = textBox_DeviceUid.Text;
            selectedConfig.EMAIL = textBox_Id.Text;
            selectedConfig.PASSWORD = textBox_Pw.Text;

            selectedConfig.spam = checkBox_Spam.Checked;
            selectedConfig.spamRange = Convert.ToInt32(numericUpDown_SpamRange.Value);
            selectedConfig.spamCount = Convert.ToInt32(numericUpDown_SpamCount.Value);
            selectedConfig.limit = checkBox_Limit.Checked;
            selectedConfig.limitRange = Convert.ToInt32(numericUpDown_LimitRange.Value);
            selectedConfig.limitCount = Convert.ToInt32(numericUpDown_LimitCount.Value);

            selectedConfig.minCheckExit = Convert.ToInt32(numericUpDown_CheckMinExit.Value);

            var data = Newtonsoft.Json.JsonConvert.SerializeObject(selectedConfig , Newtonsoft.Json.Formatting.Indented);

            File.WriteAllText(fileName , data);

            PrintLog($"config 저장 : {fileName}");
        }

        private void button_AddNickBlack_Click(object sender, EventArgs e)
        {
            var addValeu = textBox_NickBlack.Text.Trim();
            if (string.IsNullOrEmpty(addValeu))
                return;

            PrintLog($"{addValeu} 추방아이디에 추가");

            selectedConfig.NickBlacklist.Add(addValeu);
            dataGridView_NickBlackList.DataSource = null;
            dataGridView_NickBlackList.DataSource = selectedConfig.NickBlacklist.Select(p => new { 추방아이디 = p }).ToList();
            dataGridView_NickBlackList.Refresh();
        }

        private void button_DelNickBlack_Click(object sender, EventArgs e)
        {
            if (dataGridView_NickBlackList.CurrentCell == null)
                return;

            int selectedIndex = dataGridView_NickBlackList.CurrentCell.RowIndex;
            string target = selectedConfig.NickBlacklist.ElementAt(selectedIndex);
            PrintLog($"{target} 추방아이디 삭제");
            selectedConfig.NickBlacklist.RemoveAt(selectedIndex);

            dataGridView_NickBlackList.DataSource = null;
            dataGridView_NickBlackList.DataSource = selectedConfig.NickBlacklist.Select(p => new { 추방아이디 = p }).ToList();
            dataGridView_NickBlackList.Refresh();
        }
    }
}
