﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigManager
{
    public partial class RoomInfoController : UserControl
    {
        public RoomBaninfo roomInfo = null;
        public RoomInfoController()
        {
            InitializeComponent();
        }

        public void SetInfo(RoomBaninfo info)
        {
            this.roomInfo = info;

            this.textBox_RoomName.Text = this.roomInfo.roomName;

            this.dataGridView_BanWord.DataSource = null;
            this.dataGridView_BanWord.DataSource = this.roomInfo.banWord.Select(p => new { 문자 = p }).ToList(); ;
            this.dataGridView_BanWord.Refresh();        
        }

        private void button_AddBanWord_Click(object sender, EventArgs e)
        {
            var addValeu = textBox_BanWord.Text.Trim();
            if (string.IsNullOrEmpty(addValeu))
                return;

            Global.PrintLog($"{addValeu} 문자 추가 in {roomInfo.roomName}");

            this.roomInfo.banWord.Add(addValeu);
            dataGridView_BanWord.DataSource = null;
            dataGridView_BanWord.DataSource = this.roomInfo.banWord.Select(p => new { 문자 = p }).ToList();
            dataGridView_BanWord.Refresh();
        }

        private void textBox_RoomName_TextChanged(object sender, EventArgs e)
        {
            this.roomInfo.roomName = textBox_RoomName.Text;
        }

        private void button_DelBanWord_Click(object sender, EventArgs e)
        {
            if (dataGridView_BanWord.CurrentCell == null)
                return;

            int selectedIndex = dataGridView_BanWord.CurrentCell.RowIndex;
            string target = this.roomInfo.banWord.ElementAt(selectedIndex);
            Global.PrintLog($"{target} 문자 삭제");
            this.roomInfo.banWord.RemoveAt(selectedIndex);

            dataGridView_BanWord.DataSource = null;
            dataGridView_BanWord.DataSource = this.roomInfo.banWord.Select(p => new { 문자 = p }).ToList();
            dataGridView_BanWord.Refresh();
        }
    }
}
