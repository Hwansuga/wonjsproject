﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

class SecureWon
{
    private static readonly SecureString LOGIN_URL = new SecureString();
    private static readonly SecureString SSL_PUBLICKEY = new SecureString();

    private static string fingerPrint = string.Empty;

    public static string Value()
    {
        if (string.IsNullOrEmpty(fingerPrint))
        {
            fingerPrint = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + biosId() + "\nBASE >> " + baseId() + "\nVIDEO >> " + videoId()
                                 //+"\nDISK >> "+ diskId() + "\nVIDEO >> " + videoId() +"\nMAC >> "+ macId()
                                 );
        }
        return fingerPrint;
    }

    //First enabled network card ID
    private static string macId()
    {
        return identifier("Win32_NetworkAdapterConfiguration", "MACAddress", "IPEnabled");
    }

    private static string identifier(string wmiClass, string wmiProperty, string wmiMustBeTrue)
    {
        string result = "";
        System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
        System.Management.ManagementObjectCollection moc = mc.GetInstances();
        foreach (System.Management.ManagementObject mo in moc)
        {
            if (mo[wmiMustBeTrue].ToString() == "True")
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
        }
        return result;
    }

    private static string GetHash(string s)
    {
        MD5 sec = new MD5CryptoServiceProvider();
        ASCIIEncoding enc = new ASCIIEncoding();
        byte[] bt = enc.GetBytes(s);
        return GetHexString(sec.ComputeHash(bt));
    }
    private static string GetHexString(byte[] bt)
    {
        string s = string.Empty;
        for (int i = 0; i < bt.Length; i++)
        {
            byte b = bt[i];
            int n, n1, n2;
            n = (int)b;
            n1 = n & 15;
            n2 = (n >> 4) & 15;
            if (n2 > 9)
                s += ((char)(n2 - 10 + (int)'A')).ToString();
            else
                s += n2.ToString();
            if (n1 > 9)
                s += ((char)(n1 - 10 + (int)'A')).ToString();
            else
                s += n1.ToString();
            if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
        }
        return s;
    }

    private static string identifier(string wmiClass, string wmiProperty)
    {
        string result = "";
        System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
        System.Management.ManagementObjectCollection moc = mc.GetInstances();
        foreach (System.Management.ManagementObject mo in moc)
        {
            //Only get the first one
            if (result == "")
            {
                try
                {
                    result = mo[wmiProperty].ToString();
                    break;
                }
                catch
                {
                }
            }
        }
        return result;
    }

    private static string cpuId()
    {
        //Uses first CPU identifier available in order of preference
        //Don't get all identifiers, as very time consuming
        string retVal = identifier("Win32_Processor", "UniqueId");
        if (retVal == "") //If no UniqueID, use ProcessorID
        {
            retVal = identifier("Win32_Processor", "ProcessorId");
            if (retVal == "") //If no ProcessorId, use Name
            {
                retVal = identifier("Win32_Processor", "Name");
                if (retVal == "") //If no Name, use Manufacturer
                {
                    retVal = identifier("Win32_Processor", "Manufacturer");
                }
                //Add clock speed for extra security
                retVal += identifier("Win32_Processor", "MaxClockSpeed");
            }
        }
        return retVal;
    }

    private static string biosId()
    {
        return identifier("Win32_BIOS", "Manufacturer")
        + identifier("Win32_BIOS", "SMBIOSBIOSVersion")
        + identifier("Win32_BIOS", "IdentificationCode")
        + identifier("Win32_BIOS", "SerialNumber")
        + identifier("Win32_BIOS", "ReleaseDate")
        + identifier("Win32_BIOS", "Version");
    }
    //Main physical hard drive ID
    private static string diskId()
    {
        return identifier("Win32_DiskDrive", "Model")
        + identifier("Win32_DiskDrive", "Manufacturer")
        + identifier("Win32_DiskDrive", "Signature")
        + identifier("Win32_DiskDrive", "TotalHeads");
    }
    //Motherboard ID
    private static string baseId()
    {
        return identifier("Win32_BaseBoard", "Model")
        + identifier("Win32_BaseBoard", "Manufacturer")
        + identifier("Win32_BaseBoard", "Name")
        + identifier("Win32_BaseBoard", "SerialNumber");
    }
    //Primary video controller ID
    private static string videoId()
    {
        return identifier("Win32_VideoController", "DriverVersion")
        + identifier("Win32_VideoController", "Name");
    }

    private static string LoadData()
    {
        string serial = "Error";

        string DirPath = "C:\\ProgramData\\kakao";
        string ReadFile = DirPath + "\\APPS.bin";
        FileInfo _finfoLD = new FileInfo(ReadFile);
        //파일 있는지 확인 있을때(true), 없으면(false)
        if (_finfoLD.Exists)
        {
            // 파일이 있을때 동작

            StreamReader rd = new StreamReader(ReadFile, System.Text.Encoding.UTF8);
            try
            {
                // 마지막이 될 때까지 루프
                while (!rd.EndOfStream)
                {
                    // 한 라인을 읽는다
                    serial = rd.ReadLine();
                    break;
                }
            }
            catch { }

            // StreamReader는 사용 후 반드시 닫는다
            rd.Close();
        }

        return serial;
    }

    private static void SaveData(string serial)
    {

        string DirPath = "C:\\ProgramData\\kakao";
        string ReadFile = DirPath + "\\APPS.bin";
        FileInfo _finfoLD = new FileInfo(ReadFile);
        if (_finfoLD.Exists != true) Directory.CreateDirectory(DirPath);

        FileStream SaveFile = new FileStream(ReadFile, System.IO.FileMode.Create, System.IO.FileAccess.Write);

        StreamWriter sd = new StreamWriter(SaveFile, System.Text.Encoding.UTF8); //엑셀 CSV 로드 가능하도록 UTF8 인코딩

        //본문 저장
        sd.WriteLine(serial);
        sd.Close();

    }

    private static string Sha512(string plainText)
    {
        var result = string.Empty;

        foreach (var item in new SHA512Managed().ComputeHash(Encoding.UTF8.GetBytes(plainText)))
        {
            result += item.ToString("x2");
        }

        return result;
    }

    public static bool ReqAuth(string url_)
    {
        ServicePointManager.DefaultConnectionLimit = int.MaxValue;
        //SecureString 만들기
        (url_).ToCharArray().ToList().ForEach(x => LOGIN_URL.AppendChar(x));

        while (true)
        {
            //사용자 계정 아이디
            var theID = string.Empty;
            //사용자 계정 비밀번호
            var thePW = string.Empty;

            // 저장 // 불러오기
            // 처음 일경우 값을 저장 하기
            string unique = LoadData();

            if (unique == "Error")
            {
                unique = Guid.NewGuid().ToString();
                SaveData(unique);
            }

            theID = unique; //ConsoleInput.ReadLine();
            thePW = "q123456789"; //ConsoleInput.ReadLinePassword();

            if (string.IsNullOrEmpty(theID) || string.IsNullOrEmpty(thePW))
                continue;

            var nowDateTimeString = DateTime.UtcNow.ToString("yyyyMMddHHmm");
            //var uniqueContentForUser = $"{nowDateTimeString.Substring(0, nowDateTimeString.Length - 1)}{theID}";
            var uniqueContentForUser = $"{theID}";
            var theSecurityCode = Util.Sha512($"{uniqueContentForUser}login");

            var thePostDataString = $"username={theID}&password={thePW}&securitycode={theSecurityCode}";
            var thePostDataBytes = Encoding.UTF8.GetBytes(thePostDataString);

            //웹 요청 준비
            var theWebRequest = (HttpWebRequest)WebRequest.Create(Util.SecureStringToString(LOGIN_URL));
            theWebRequest.Proxy = null;
            theWebRequest.Method = WebRequestMethods.Http.Post;
            theWebRequest.ContentType = "application/x-www-form-urlencoded";
            theWebRequest.ContentLength = thePostDataBytes.Length;
            theWebRequest.UserAgent = "HypnotikAuthSystem";

            //PostData 입력
            using (var theRequestStream = theWebRequest.GetRequestStream())
            {
                theRequestStream.Write(thePostDataBytes, 0, thePostDataBytes.Length);
                theRequestStream.Close();
            }

            HttpWebResponse theWebResponse;

            try
            {
                //웹 요청 실행
                theWebResponse = (HttpWebResponse)theWebRequest.GetResponse();
            }
            catch (WebException e)
            {
                MessageBox.Show(e.ToString());
                continue;
            }

            var theResponseString = string.Empty;

            //웹 응답 데이터 가져오기
            using (var theResponseStream = theWebResponse.GetResponseStream())
            {
                using (var theStreamReader = new StreamReader(theResponseStream))
                {
                    theResponseString = theStreamReader.ReadToEnd();
                }
            }


            var theUniqueCodeToLoginSuccess = Util.Sha512($"{uniqueContentForUser}alright");

            if (!theResponseString.Contains(theUniqueCodeToLoginSuccess))
            {
                //로그인 여부 확인
                Clipboard.SetText(unique);
                MessageBox.Show("관리자에게 문의 하세요 / 인증번호가 복사되었습니다.", "인증실패");               
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

