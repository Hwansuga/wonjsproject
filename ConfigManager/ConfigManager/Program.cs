using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigManager
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if RELEASE
            if (SecureWon.ReqAuth("http://nsw1911.dothome.co.kr/loginsimple.php") == false)
            {
                Process.GetCurrentProcess().Kill();
                return;
            }
#endif



            string mutexKey = "{AA9DE4EB-22DC-489E-BF66-F3C26B9A57AF}";
            //check duplication
            if (Util.CheckDuplication(mutexKey))
                return;

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_Main());
        }
    }
}
