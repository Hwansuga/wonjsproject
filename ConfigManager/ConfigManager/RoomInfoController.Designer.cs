﻿
namespace ConfigManager
{
    partial class RoomInfoController
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_RoomName = new System.Windows.Forms.TextBox();
            this.dataGridView_BanWord = new System.Windows.Forms.DataGridView();
            this.button_DelBanWord = new System.Windows.Forms.Button();
            this.button_AddBanWord = new System.Windows.Forms.Button();
            this.textBox_BanWord = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_BanWord)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.59794F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.40206F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_RoomName, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(194, 28);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "방 이름";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_RoomName
            // 
            this.textBox_RoomName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_RoomName.Location = new System.Drawing.Point(74, 3);
            this.textBox_RoomName.Name = "textBox_RoomName";
            this.textBox_RoomName.Size = new System.Drawing.Size(117, 23);
            this.textBox_RoomName.TabIndex = 1;
            this.textBox_RoomName.TextChanged += new System.EventHandler(this.textBox_RoomName_TextChanged);
            // 
            // dataGridView_BanWord
            // 
            this.dataGridView_BanWord.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_BanWord.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_BanWord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_BanWord.ColumnHeadersVisible = false;
            this.dataGridView_BanWord.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridView_BanWord.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView_BanWord.Location = new System.Drawing.Point(0, 28);
            this.dataGridView_BanWord.MultiSelect = false;
            this.dataGridView_BanWord.Name = "dataGridView_BanWord";
            this.dataGridView_BanWord.ReadOnly = true;
            this.dataGridView_BanWord.RowHeadersVisible = false;
            this.dataGridView_BanWord.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dataGridView_BanWord.RowTemplate.Height = 25;
            this.dataGridView_BanWord.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_BanWord.Size = new System.Drawing.Size(194, 242);
            this.dataGridView_BanWord.TabIndex = 2;
            // 
            // button_DelBanWord
            // 
            this.button_DelBanWord.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_DelBanWord.Location = new System.Drawing.Point(0, 316);
            this.button_DelBanWord.Name = "button_DelBanWord";
            this.button_DelBanWord.Size = new System.Drawing.Size(194, 23);
            this.button_DelBanWord.TabIndex = 7;
            this.button_DelBanWord.Text = "삭제";
            this.button_DelBanWord.UseVisualStyleBackColor = true;
            this.button_DelBanWord.Click += new System.EventHandler(this.button_DelBanWord_Click);
            // 
            // button_AddBanWord
            // 
            this.button_AddBanWord.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_AddBanWord.Location = new System.Drawing.Point(0, 293);
            this.button_AddBanWord.Name = "button_AddBanWord";
            this.button_AddBanWord.Size = new System.Drawing.Size(194, 23);
            this.button_AddBanWord.TabIndex = 6;
            this.button_AddBanWord.Text = "추가";
            this.button_AddBanWord.UseVisualStyleBackColor = true;
            this.button_AddBanWord.Click += new System.EventHandler(this.button_AddBanWord_Click);
            // 
            // textBox_BanWord
            // 
            this.textBox_BanWord.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox_BanWord.Location = new System.Drawing.Point(0, 270);
            this.textBox_BanWord.Name = "textBox_BanWord";
            this.textBox_BanWord.Size = new System.Drawing.Size(194, 23);
            this.textBox_BanWord.TabIndex = 5;
            // 
            // RoomInfoController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button_DelBanWord);
            this.Controls.Add(this.button_AddBanWord);
            this.Controls.Add(this.textBox_BanWord);
            this.Controls.Add(this.dataGridView_BanWord);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "RoomInfoController";
            this.Size = new System.Drawing.Size(194, 344);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_BanWord)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_RoomName;
        private System.Windows.Forms.DataGridView dataGridView_BanWord;
        private System.Windows.Forms.Button button_DelBanWord;
        private System.Windows.Forms.Button button_AddBanWord;
        private System.Windows.Forms.TextBox textBox_BanWord;
    }
}
